---
title: Cgroup v2 Notes
date: 2020-11-12 09:48:43
tags: cgroup
---

Cgroup v2 相关的学习笔记

## 开启 cgroup v2

当前 systemd 支持三种 cgroup 模式，分别是

1. legacy， 采用 cgroup v1
2. hybrid，混杂模式，既挂载 cgroup v1 也挂载 cgroup v2， 但是在该模式下，cgroup v2 下不使能任何 controller，不用于资源管理,参考[systemd 模式说明](https://github.com/systemd/systemd/pull/10161/files)
3. unified, 纯粹使用 cgroup v2

使能 unified 模式方法：

首先，修改内核启动选项，为 GRUB_CMDLINE_LINUX 增加选项 cgroup_no_v1=all

```
# cat /etc/default/grub
GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0 console=ttyS0,115200 console=tty0 panic=5 cgroup_no_v1=all"
```

使用"update-grub"命令更新 grub 配置，然后重启系统。
重启后，就可以看到，cgroup 使用 cgroup v2 挂载

```
# mount
mount  | grep cgroup
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,mode=755)
cgroup on /sys/fs/cgroup/unified type cgroup2 (rw,nosuid,nodev,noexec,relatime)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,xattr,name=systemd)

```

## cgroup v2 挂载点文件说明

进入到/sys/fs/cgroup/unified 目录下，这里是 cgroup v2 的根目录，在该目录下，可以看到

```
# ls -al
-r--r--r--  1 root root   0 Nov 12 10:14 cgroup.controllers
-rw-r--r--  1 root root   0 Nov 12 10:14 cgroup.max.depth
-rw-r--r--  1 root root   0 Nov 12 10:14 cgroup.max.descendants
-rw-r--r--  1 root root   0 Nov 12 10:14 cgroup.procs
-r--r--r--  1 root root   0 Nov 12 10:14 cgroup.stat
-rw-r--r--  1 root root   0 Nov 12 10:11 cgroup.subtree_control
-rw-r--r--  1 root root   0 Nov 12 10:14 cgroup.threads

```

其中比较重要的是这两个文件：

1. cgroup.controllers 文件，只读的文件，里面的内容表示该层支持的 cgroup 控制器
2. cgroup.subtree_control 文件，可读写文件，用于控制该层的子层使能哪些 cgroup 控制器，可以往里面写入的内容是 cgroup.controllers 的子集

## 父层使能限制的资源，子层未使能该资源控制器的话，行为测试

由于每层的控制器可以通过 subtree.controller 分别开启，因此存在上层开启某些控制器，但是下层未开启的情况。比如，存在以下层级，其中 A 层级使能了 cpu、memory 控制器，并设置了 memory 限制，B 层级仅仅使能了 cpu，未使能 memory 控制器。

```
  A(cpu,memory) - B(cpu) - C()
                         \ D()
```

A 层级使能的控制器：

```
cgroup/unified/A » cat cgroup.controllers
cpu memory
```

B 层级使能的控制器：

```
unified/A/B » cat cgroup.controllers
cpu
```

在 A 层级对 memory 进行限制，比如设置 memory.max

```
cgroup/unified/A » echo 10240 > memory.max
cgroup/unified/A » cat memory.max
8192
```

在 B 层级上，由于未使能 memory，这里我们测试下，内存限制是否生效

```
unified/A/B » echo $$ > cgroup.procs
unified/A/B » zsh
unified/A/B » echo $$
26034
unified/A/B » echo $$ > cgroup.procs
unified/A/B » echo $$
24914
```

dmesg 查看，限制生效，但是不是 OOM

```
[45655.834884] SLUB: Unable to allocate memory on node -1, gfp=0x14080c0(GFP_KERNEL|__GFP_ZERO)
[45655.836826]   cache: vm_area_struct(295:A), object size: 208, buffer size: 208, default order: 0, min order: 0
[45655.838958]   node 0: slabs: 0, objs: 0, free: 0
```

在 B 层级使能 memory，再测试下，触发 OOM

```
unified/A/B » cat cgroup.controllers
cpu memory
unified/A/B » zsh
unified/A/B » echo $$
26549
unified/A/B » echo $$ > cgroup.procs
[1]    26549 killed     zsh
```

查看 dmesg，发生 OOM

```
[45827.778104] Memory cgroup out of memory: Kill process 26549 (zsh) score 741000 or sacrifice child
[45827.780199] Killed process 26549 (zsh) total-vm:44548kB, anon-rss:1740kB, file-rss:4236kB, shmem-rss:0kB
[45827.782562] oom_reaper: reaped process 26549 (zsh), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
```

在 C\D 层级上，由于未使能 memory, 测试内存限制是否生效

```
A/B/C » zsh
A/B/C » echo $$
23971
A/B/C » echo $$ > cgroup.procs
A/B/C » echo $$
2106
```

dmesg 查看，确认申请内存失败，但是限制还是生效的

```
[45057.975219] SLUB: Unable to allocate memory on node -1, gfp=0x14080c0(GFP_KERNEL|__GFP_ZERO)
[45057.977400]   cache: vm_area_struct(326:B), object size: 208, buffer size: 208, default order: 0, min order: 0
[45057.979747]   node 0: slabs: 0, objs: 0, free: 0
```

在 C\D 层级上，使能 memory, 测试内存限制生效,触发 OOM

```
unified/A/B » echo "+memory" > cgroup.subtree_control
unified/A/B » cd C
A/B/C » cat cgroup.controllers
cpu memory
A/B/C » zsh
A/B/C » cat memory.max
max
A/B/C » zsh
A/B/C » echo $$ > cgroup.procs
[1]    25131 killed     zsh
```

## No Internal Process Constraint，行为测试

在内核的文档中描述了“No Internal Process Constraint”无内部进程的约束。我的理解是进程只能接入到 cgroup 树中的叶子节点，而不能加入到中间的枝干节点。
比如我们存在这个树状结构。

```
  A - B - C()
```

当我们将进程放入 B 层级后，再尝试将新进程加入底层的 C 层，就会报操作不允许,这种情况下，相当于 C 层级不能加入任何进程了。

```
unified/A/B » sleep 1000 &
[1] 23775
unified/A/B » echo 23775 > ./cgroup.procs
unified/A/B » sleep 1000 &
[2] 23819
unified/A/B » echo 23819 > ./C/cgroup.procs
echo: write error: operation not supported
```

相反地，我们先将进程加入 C 层级，再尝试将进程加入 B 层级，也会得到错误。

```
A/B/C » sleep 1000 &
[1] 24274
A/B/C » echo 24274 > cgroup.procs
A/B/C » cd ..
unified/A/B » sleep 1000 &
[3] 24336
unified/A/B » echo 24336 > ./cgroup.procs
echo: write error: device or resource busy
```

## 参考资料

- [内核 cgroup v2 文档](https://www.kernel.org/doc/Documentation/cgroup-v2.txt)
