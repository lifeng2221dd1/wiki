---
title: dig_into_kernel_namespace
date: 2021-03-06 15:44:31
tags: namespace
---

Namespace 是一个资源局部化的集合，由各个子系统的命名空间组合而成。每个子系统的对象由原来的全局唯一的一个实例局部化为多个实例。实例之间互不干扰，一个实例不能访问另外一个空间的元素。

## 数据结构

命名空间在内核里被抽象成为一个数据结构 struct nsproxy， 其定义如下

```c
    struct nsproxy {
        atomic_t count;
        struct uts_namespace *uts_ns;
        struct ipc_namespace *ipc_ns;
        struct mnt_namespace *mnt_ns;
        struct pid_namespace *pid_ns_for_children;
        struct net 	     *net_ns;
        struct time_namespace *time_ns;
        struct time_namespace *time_ns_for_children;
        struct cgroup_namespace *cgroup_ns;
    };
```

每个进程的task_struct 中有一个 struct nsproxy的指针，指向该进程所属的namespace。共享同一个命名空间（所有子系统均共享）的进程指向同一个nsproxy实例，通过引用计数来确定使用者的数目。当使用clone系统调用带NEW_NS标志时，就会产生另外一个新的nsproxy实例，然后新的进程的task_struct中的nsproxy指向这个新的实例。


```c
struct task_struct {
    ...
    /* Namespaces: */
    struct nsproxy			*nsproxy;
    ....
}
```

初始化时，会初始化一个全局的命名空间。

```c
struct nsproxy init_nsproxy = {
    .count = ATOMIC_INIT(1),
    .uts_ns = &init_uts_ns,
    #if defined(CONFIG_POSIX_MQUEUE) || defined(CONFIG_SYSVIPC)
    .ipc_ns = &init_ipc_ns,
    #endif
    .mnt_ns = NULL,
    .pid_ns_for_children = &init_pid_ns,
    #ifdef CONFIG_NET
    .net_ns = &init_net,
    #endif
    #ifdef CONFIG_CGROUPS
    .cgroup_ns = &init_cgroup_ns,
    #endif
    #ifdef CONFIG_TIME_NS
    .time_ns = &init_time_ns,
    .time_ns_for_children = &init_time_ns,
    #endif
};
```

在初始化struct task_struct init_task时，将init_task的nsproxy设置为init_nsproxy。


## UTS 命名空间

UTS是一个比较简单的命名空间，其数据结构定义如下：

```c
struct new_utsname {
	char sysname[__NEW_UTS_LEN + 1];
	char nodename[__NEW_UTS_LEN + 1];
	char release[__NEW_UTS_LEN + 1];
	char version[__NEW_UTS_LEN + 1];
	char machine[__NEW_UTS_LEN + 1];
	char domainname[__NEW_UTS_LEN + 1];
};
struct uts_namespace {
	struct new_utsname name;
	struct user_namespace *user_ns;
	struct ucounts *ucounts;
	struct ns_common ns;
} __randomize_layout;
```
UTS namespace 定义了一个初始化的实例：

```c
struct uts_namespace init_uts_ns = {
	.ns.count = REFCOUNT_INIT(2),
	.name = {
		.sysname	= UTS_SYSNAME,
		.nodename	= UTS_NODENAME,
		.release	= UTS_RELEASE,
		.version	= UTS_VERSION,
		.machine	= UTS_MACHINE,
		.domainname	= UTS_DOMAINNAME,
	},
	.user_ns = &init_user_ns,
	.ns.inum = PROC_UTS_INIT_INO,
#ifdef CONFIG_UTS_NS
	.ns.ops = &utsns_operations,
#endif
};
```
当clone系统调用被指定创建一个新的UTS空间时，主要调用流程如下：

```c
copy_process
    ---> copy_namespaces
        ---> create_new_namespaces
            ---> copy_utsname
                ---> 如果没有设置CLONE_NEWUTS， 返回老的uts ns
                ---> clone_uts_ns
                    ---> uts_ns = kmem_cache_alloc(uts_ns_cache, GFP_KERNEL);
```



