---
title: iSulad 重构经验总结
date: 2020-08-29 16:09:02
tags: iSulad
---
# iSulad 重构经验总结

前段时间对 iSulad 的代码目录结构进行了一次大规模的重构。在重构的过程中，有些经验进行下总结回顾,方便以后查阅。

## 为什么要重构

重构应该是在整个开发过程中持续进行的一项质量活动。重构即是在不改变软件外在表现行为的前提下，通过修改代码的内部实现，达到优化内部结构、方便阅读理解、甚至提升性能的代码修改活动。我们在开发的过程中，经常会遇到以下问题，我认为都是进行重构的好时机：

1. 当我修改 1 个简单问题时，发现代码逻辑不清楚，在多个模块的代码中去查找，花费很多时间去重新理解代码逻辑，则说明当前的代码是难以理解的，可以进行小规模的重构。
2. 当我新增新特性时，发现当前的代码设计、模块划分，让我很难新加特性，加到哪里都不合适，这时候就需要思考，代码架构设计、模块划分是否可以进行重构，能够让未来新添加特性时，能够更轻松愉悦，这种场景下，重构的代码量一般比较大。
3. 代码 review 时，如果发现新的提交，增加了原代码的复杂度。我们就需要考虑，这个修改提交是否有重构优化的必要。如果我们都能够做到，提交的新代码都比原来的代码逻辑更加清晰、复杂度更低，那么代码质量就会越来越高。

重构的目标就是让开发人员更加轻松愉悦的 coding。

## 重构的前提条件

### 完备的验证机制

重构的一个重要原则是，不要改变软件对外的表现，包括性能、功能。如果我们进行的重构破坏了软件原来的功能、降低了软件的性能，那么我们就不是在进行重构，而是在引入问题。因此在进行重构前，要搭建好完整的功能、性能验证环境，以进行验证重构后的软件对外行为的一致性。
比较好的实践是根据要重构的内容，在重构前，首先考虑补充完整对应的功能、性能测试用例。然后再进行重构，在重构的过程中，可以使用用例进行手动验证。重构完成后，将用例、代码一起提交到门禁中，将用例保存归档，以便后续重构时使用。

### 辅助分析工具

一个好的分析工具，能够方便地展示软件代码的内部结构、模块间的依赖关系，以便能够对代码的现状进行分析和度量。分析架构设计与代码实际现状的差距。这里推荐使用 structure 101 工具。工具能够从元素以及元素之间的关系对软件的架构进行度量。如下图，
<img src="struct101.png" alt="struct101" style="zoom:100%;" />
可以展示出当前代码存在的不合理依赖关系，标识不合理依赖的调用次数。

### 代码架构设计

在实际修改代码前，应当进行代码设计。特别是针对代码架构层级的重构，需要对软件的架构进行建模，以确定软件的架构分层是否合理、模块间的领域边界是否合理。在这里进行建模时，要把视角从原来的设计中抽离出来，按照业务合理的分层进行划分。
iSulad 按照业务逻辑进行架构分层，各逻辑分层划分以及对应的职责如下：

| **层次**   | **职责划分**                                                                           |
| ---------- | -------------------------------------------------------------------------------------- |
| 用户界面   | 负责向用户展现信息以及解释用户命令                                                     |
| 接口层     | 客户端服务端通信接口、CRI 接口的定义与实现                                             |
| 应用层     | 调用领域层的接口，实现对应的业务应用                                                   |
| 领域层     | 本层包含关于领域的信息，这是 iSulad 软件的核心所在。包含多种模块，业务逻辑实际的执行层 |
| 基础设施层 | 本层作为其他层的支撑库。它提供各种工具函数供其他层次调用使用                           |

根据上述逻辑分层的设计思想，iSulad 对应源码目录架构应该设计为:

<img src="directory.jpg" alt="iSulad-img-single" style="zoom:100%;" />

## iSulad 重构的经验

### 目录合理规划，体现架构设计

重构前，iSulad src 代码目录没有合理的规划、分类。比如工具函数文件和模块函数文件混杂，导致开发视图和逻辑视图不一致。属于基础设施层的 tar、console、http、map 等工具模块文件，与领域层模块实现 image、services、engines 同属于同一目录层级。

```sh
...
├── api
├── CMakeLists.txt
├── cmd
├── config
├── connect
├── console
├── constants.h
├── container_def.c
├── container_def.h
├── contrib
├── cpputils
├── cutils
├── engines
├── error.c
├── error.h
├── filters.c
├── filters.h
├── http
├── image
├── json
├── liblcrc.c
├── liblcrc.h
├── liblcrd.c
├── liblcrd.h
├── linked_list.h
├── log.c
├── log.h
├── mainloop.c
├── mainloop.h
├── map
├── namespace.c
├── namespace.h
├── pack_config.c
├── pack_config.h
├── path.c
├── path.h
├── plugin
├── runtime
├── services
├── sha256
├── sysctl_tools.c
├── sysctl_tools.h
├── tar
├── types_def.c
├── types_def.h
└── websocket
```

根据软件分层架构设计，识别各个文件、文件夹属于架构分层中的哪个层级后，进行目录的调整、合并。例如 console、cpputils、cutils、http、sha256、tar 等属于工具类函数，统一放到 utils 目录下

```sh
├── api
│   ├── services
│   └── types
├── client
│   ├── CMakeLists.txt
│   ├── connect
│   ├── libisula.c
│   └── libisula.h
├── CMakeLists.txt
├── cmd
│   ├── CMakeLists.txt
│   ├── command_parser.c
│   ├── command_parser.h
│   ├── isula
│   ├── isulad
│   └── isulad-shim
├── common
│   ├── CMakeLists.txt
│   └── constants.h
├── contrib
│   ├── config
│   ├── env_checkconfig
│   ├── init
│   └── sysmonitor
├── daemon
│   ├── CMakeLists.txt
│   ├── common
│   ├── config
│   ├── entry
│   ├── executor
│   └── modules
└── utils
    ├── buffer
    ├── CMakeLists.txt
    ├── console
    ├── cpputils
    ├── cutils
    ├── http
    ├── sha256
    └── tar
```

### 明确模块边界

重构前，iSulad 内部模块边界不清晰，导致部分业务职责划分不合理，缺少接口约束，不同模块间调用随意。
重构之前领域层内部模块调用关系如下：
<img src="old.png" alt="struct101" style="zoom:100%;" />
可以看到不同模块之间随意调用，存在循环的依赖关系，如 image 和 execution 两个模块之间，存在互相调用。
首先指定模块划分以及确定模块对外接口，根据模块划分，整改不合理代码。如 image 和 graphdriver 间不应该存在互相调用，而是 image 单向调用 graphdriver 的接口。driver 模块应当明确对外提供的接口，上层 image 仅能调用 driver 导出的接口。

driver.h 对外接口定义：

```c
int graphdriver_init(const struct storage_module_init_options *opts);

int graphdriver_create_rw(const char *id, const char *parent, struct driver_create_opts *create_opts);

int graphdriver_create_ro(const char *id, const char *parent, const struct driver_create_opts *create_opts);

int graphdriver_rm_layer(const char *id);

char *graphdriver_mount_layer(const char *id, const struct driver_mount_opts *mount_opts);

int graphdriver_umount_layer(const char *id);

bool graphdriver_layer_exists(const char *id);

int graphdriver_apply_diff(const char *id, const struct io_read_wrapper *content);

struct graphdriver_status *graphdriver_get_status(void);

void free_graphdriver_status(struct graphdriver_status *status);

void free_graphdriver_mount_opts(struct driver_mount_opts *opts);

int graphdriver_cleanup(void);

int graphdriver_try_repair_lowers(const char *id, const char *parent);

container_inspect_graph_driver *graphdriver_get_metadata(const char *id);

int graphdriver_get_layer_fs_info(const char *id, imagetool_fs_info *fs_info);
```

整改后调用关系：
<img src="call_map.jpg" alt="struct101" style="zoom:100%;" />

### 清晰的命名，拒绝笼统

给文件、模块、函数、变量取名字对于程序员来说，是最简单、最常做的事情。可是取的名字能够做到清晰的表达，是比较难的。命名应当做到具有明确的意图，避免使用笼统的文件、目录命名，需要按照其职责来进行划分。
以 iSulad 重构为例，重构前存在一个文件名为 libisulad.c 的文件，里面定义的函数可以被 daemon 调用到，这就导致了，当程序员要新加 1 个函数时，当找不到合适的位置，都会加入到 libisulad.c, 似乎放在这里最合适，加的函数越来越多，导致这个文件的功能越来越多样。重构前该文件的功能就已经包含一些完全不相关的结构体的释放、设置 errormsg isulad_set_error_message 的接口，可以预见的，这个文件如果不进行重构，其功能会越来越多。

```c
void container_log_config_free(struct container_log_config *conf);

void isulad_events_request_free(struct isulad_events_request *request);

void isulad_copy_from_container_request_free(struct isulad_copy_from_container_request *request);

void isulad_copy_from_container_response_free(struct isulad_copy_from_container_response *response);

void isulad_set_error_message(const char *format, ...);

void isulad_try_set_error_message(const char *format, ...);

void isulad_append_error_message(const char *format, ...);

void isulad_container_rename_request_free(struct isulad_container_rename_request *request);

void isulad_container_rename_response_free(struct isulad_container_rename_response *response);

void isulad_container_resize_request_free(struct isulad_container_resize_request *request);

void isulad_container_resize_response_free(struct isulad_container_resize_response *response);

void isulad_logs_request_free(struct isulad_logs_request *request);
void isulad_logs_response_free(struct isulad_logs_response *response);

void isulad_events_format_free(struct isulad_events_format *value);
```

通过合理的拆分文件，将设置 error_msg 的接口单独拆分出来，单独形成 err_msg.c、err_msg.h，对外提供单一的接口。

```c
void isulad_set_error_message(const char *format, ...);

void isulad_try_set_error_message(const char *format, ...);

void isulad_append_error_message(const char *format, ...);
```

而其他结构体的释放函数，则统一挪到靠近其使用的地方，减小其作用域。

### 合理使用设计模式

针对 iSulad 中需要支持多种镜像格式，包括 oci、embedded、external 等，以及镜像底层驱动需要支持 overlay、devmapper 两种。采用工厂设计模式方法，封装出 image 和 driver 接口，代码流程实现统一。

多种 image 操作抽象出通用的操作接口，以屏蔽不同 image 底层实现的细节。

```c
/* embedded */
static const struct bim_ops g_embedded_ops = {
    .init = embedded_init,
    .clean_resource = embedded_exit,
    .detect = embedded_detect,

    .prepare_rf = embedded_prepare_rf,
    .mount_rf = embedded_mount_rf,
    .umount_rf = embedded_umount_rf,
    .delete_rf = embedded_delete_rf,
    .export_rf = NULL,

    .merge_conf = embedded_merge_conf,
    .get_user_conf = embedded_get_user_conf,

    .list_ims = embedded_list_images,
    .get_image_count = NULL,
    .rm_image = embedded_remove_image,
    .inspect_image = embedded_inspect_image,
    .resolve_image_name = embedded_resolve_image_name,
    .container_fs_usage = embedded_filesystem_usage,
    .get_filesystem_info = NULL,
    .image_status = NULL,
    .load_image = embedded_load_image,
    .pull_image = NULL,

    .login = NULL,
    .logout = NULL,
    .tag_image = NULL,
    .import = NULL,
};
#endif

#ifdef ENABLE_OCI_IMAGE
static const struct bim_ops g_oci_ops = {
    .init = oci_init,
    .clean_resource = oci_exit,
    .detect = oci_detect,

    .prepare_rf = oci_prepare_rf,
    .mount_rf = oci_mount_rf,
    .umount_rf = oci_umount_rf,
    .delete_rf = oci_delete_rf,
    .export_rf = oci_export_rf,

    .merge_conf = oci_merge_conf_rf,
    .get_user_conf = oci_get_user_conf,

    .list_ims = oci_list_images,
    .get_image_count = oci_get_images_count,
    .rm_image = oci_rmi,
    .inspect_image = oci_inspect_image,
    .resolve_image_name = oci_resolve_image_name,
    .container_fs_usage = oci_container_filesystem_usage,
    .get_filesystem_info = oci_get_filesystem_info,
    .image_status = oci_status_image,
    .load_image = oci_load_image,
    .pull_image = oci_pull_rf,
    .login = oci_login,
    .logout = oci_logout,
    .tag_image = oci_tag,
    .import = oci_import,
};
#endif

/* external */
static const struct bim_ops g_ext_ops = {
    .init = ext_init,
    .clean_resource = NULL,
    .detect = ext_detect,

    .prepare_rf = ext_prepare_rf,
    .mount_rf = ext_mount_rf,
    .umount_rf = ext_umount_rf,
    .delete_rf = ext_delete_rf,
    .export_rf = NULL,

    .merge_conf = ext_merge_conf,
    .get_user_conf = ext_get_user_conf,

    .list_ims = ext_list_images,
    .get_image_count = NULL,
    .rm_image = ext_remove_image,
    .inspect_image = ext_inspect_image,
    .resolve_image_name = ext_resolve_image_name,
    .container_fs_usage = ext_filesystem_usage,
    .image_status = NULL,
    .get_filesystem_info = NULL,
    .load_image = ext_load_image,
    .pull_image = NULL,
    .login = ext_login,
    .logout = ext_logout,
    .tag_image = NULL,
    .import = NULL,
};

static const struct bim_type g_bims[] = {
#ifdef ENABLE_OCI_IMAGE
    {
        .image_type = IMAGE_TYPE_OCI,
        .ops = &g_oci_ops,
    },
#endif
    { .image_type = IMAGE_TYPE_EXTERNAL, .ops = &g_ext_ops },
#ifdef ENABLE_EMBEDDED_IMAGE
    { .image_type = IMAGE_TYPE_EMBEDDED, .ops = &g_embedded_ops },
#endif
};
```

多种 driver 操作抽象出通用的操作接口，以屏蔽不同 driver 底层实现的细节。

```c
static const struct graphdriver_ops g_overlay2_ops = {
    .init = overlay2_init,
    .create_rw = overlay2_create_rw,
    .create_ro = overlay2_create_ro,
    .rm_layer = overlay2_rm_layer,
    .mount_layer = overlay2_mount_layer,
    .umount_layer = overlay2_umount_layer,
    .exists = overlay2_layer_exists,
    .apply_diff = overlay2_apply_diff,
    .get_layer_metadata = overlay2_get_layer_metadata,
    .get_driver_status = overlay2_get_driver_status,
    .clean_up = overlay2_clean_up,
    .try_repair_lowers = overlay2_repair_lowers,
    .get_layer_fs_info = overlay2_get_layer_fs_info,
};

/* devicemapper */
#define DRIVER_DEVMAPPER_NAME "devicemapper"

static const struct graphdriver_ops g_devmapper_ops = {
    .init = devmapper_init,
    .create_rw = devmapper_create_rw,
    .create_ro = devmapper_create_ro,
    .rm_layer = devmapper_rm_layer,
    .mount_layer = devmapper_mount_layer,
    .umount_layer = devmapper_umount_layer,
    .apply_diff = devmapper_apply_diff,
    .exists = devmapper_layer_exist,
    .get_layer_metadata = devmapper_get_layer_metadata,
    .get_driver_status = devmapper_get_driver_status,
    .clean_up = devmapper_clean_up,
    .try_repair_lowers = devmapper_repair_lowers,
    .get_layer_fs_info = devmapper_get_layer_fs_info,
};

static struct graphdriver g_drivers[] = { { .name = DRIVER_OVERLAY2_NAME, .ops = &g_overlay2_ops },
    { .name = DRIVER_OVERLAY_NAME, .ops = &g_overlay2_ops },
    { .name = DRIVER_DEVMAPPER_NAME, .ops = &g_devmapper_ops }
};
```

### 模块对外接口统一管理

在重构前的代码中，各个模块对外的接口，放在模块内部的头文件定义中。这样存在什么问题呢，当新增接口时，大家都无法明确的感知接口的新增，没有感知到，也就不会有分析其是否合理，可以优化。重构后，将各个模块对外导出的接口，统一放置在 src/daemon/modules/api 目录下。当对其进行改动时，即可感知到对模块对外的接口有变更。

```sh
/home/lifeng/openeuler_iSulad/src/daemon/modules/api
daemon/modules/api ‹master› » ls -al
total 76
drwxrwxr-x  2 lifeng lifeng 4096 8月  28 15:21 .
drwxrwxr-x 12 lifeng lifeng 4096 7月  25 10:12 ..
-rw-rw-r--  1 lifeng lifeng  192 7月  25 10:12 CMakeLists.txt
-rw-------  1 lifeng lifeng 7963 8月  28 15:21 container_api.h
-rw-------  1 lifeng lifeng 1743 8月  28 15:21 events_collector_api.h
-rw-------  1 lifeng lifeng 1313 8月  28 15:21 events_sender_api.h
-rw-------  1 lifeng lifeng 1948 8月  28 15:21 event_type.h
-rw-------  1 lifeng lifeng 8322 8月  28 15:21 image_api.h
-rw-------  1 lifeng lifeng 1606 8月  28 15:21 io_handler.h
-rw-------  1 lifeng lifeng 1365 8月  28 15:21 log_gather_api.h
-rw-------  1 lifeng lifeng 3628 8月  28 15:21 plugin_api.h
-rw-------  1 lifeng lifeng 8083 8月  28 15:21 runtime_api.h
-rw-------  1 lifeng lifeng 1971 8月  28 15:21 service_container_api.h
-rw-------  1 lifeng lifeng 1051 8月  28 15:21 service_image_api.h
-rw-------  1 lifeng lifeng 2032 8月  28 15:21 specs_api.h
```
