---
title: CRIU_with_iSulad
date: 2021-04-08 09:42:46
tags: CRIU
---

# 深度暂停/恢复容器

参加鲲鹏昇腾生态培训，与高校教授们进行交流iSulad时，提及到iSulad当前支持pause、unpause功能，用于暂停容器的运行，相当与释放了CPU计算资源，但是其占用的内存仍持有着，未释放。在教学过程中，在容器中进行较大规模的计算，但是每节课的时间是有限的，当前这一节课容器中进行的计算进行到一半时，怎么能够暂停容器，等待下节课继续进行计算呢？使用pause、unpause的话，需要将容器保持在FROZEN状态到下次课程，对内存资源的消耗较大。有没有更深度的暂停容器方案，能够释放尽量多的资源，并且下次能够从上次运行的状态下快速恢复呢？

## CRIU checkpoint/restore

要实现教授的这个需求，首先想到的就是使用CRIU技术，将容器进行checkpoint，将容器中的内存、挂载点、cgroup配置等容器信息持久化到磁盘。然后restore时，通过既有的磁盘文件，恢复回来。CRIU官方针对docker是有专门的指导[CRIU-Docker](https://criu.org/Docker)，在我本地的机器（centos 7）验证是可以满足需求。

## docker 实现原理

docker 实现checkpoint、restore功能的主体在于底层执行的runtime（runc），runc提供了以下checkpoint/restore 功能。

```c
[root@localhost ~]# runc checkpoint --help
NAME:
   runc checkpoint - checkpoint a running container

USAGE:
   runc checkpoint [command options] <container-id>

Where "<container-id>" is the name for the instance of the container to be
checkpointed.

DESCRIPTION:
   The checkpoint command saves the state of the container instance.

OPTIONS:
   --image-path value           path for saving criu image files
   --work-path value            path for saving work files and logs
   --parent-path value          path for previous criu image files in pre-dump
   --leave-running              leave the process running after checkpointing
   --tcp-established            allow open tcp connections
   --ext-unix-sk                allow external unix sockets
   --shell-job                  allow shell jobs
   --lazy-pages                 use userfaultfd to lazily restore memory pages
   --status-fd value            criu writes \0 to this FD once lazy-pages is ready (default: -1)
   --page-server value          ADDRESS:PORT of the page server
   --file-locks                 handle file locks, for safety
   --pre-dump                   dump container's memory information only, leave the container running after this
   --manage-cgroups-mode value  cgroups mode: 'soft' (default), 'full' and 'strict'
   --empty-ns value             create a namespace, but don't restore its properties
   --auto-dedup                 enable auto deduplication of memory images


[root@localhost ~]# runc restore --help
NAME:
   runc restore - restore a container from a previous checkpoint

USAGE:
   runc restore [command options] <container-id>

Where "<container-id>" is the name for the instance of the container to be
restored.

DESCRIPTION:
   Restores the saved state of the container instance that was previously saved
using the runc checkpoint command.

OPTIONS:
   --console-socket value       path to an AF_UNIX socket which will receive a file descriptor referencing the master end of the console's pseudoterminal
   --image-path value           path to criu image files for restoring
   --work-path value            path for saving work files and logs
   --tcp-established            allow open tcp connections
   --ext-unix-sk                allow external unix sockets
   --shell-job                  allow shell jobs
   --file-locks                 handle file locks, for safety
   --manage-cgroups-mode value  cgroups mode: 'soft' (default), 'full' and 'strict'
   --bundle value, -b value     path to the root of the bundle directory
   --detach, -d                 detach from the container's process
   --pid-file value             specify the file to write the process id to
   --no-subreaper               disable the use of the subreaper used to reap reparented processes
   --no-pivot                   do not use pivot root to jail process inside rootfs.  This should be used whenever the rootfs is on top of a ramdisk
   --empty-ns value             create a namespace, but don't restore its properties
   --auto-dedup                 enable auto deduplication of memory images
   --lazy-pages                 use userfaultfd to lazily restore memory pages
```

实现流程：

```             
                  checkpoint管理(create\ls\rm)---->containerd---->runc(checkpoint 创建checkpoint目录)
                 /
                /
user---->docker---start本容器(--checkpoint/--checkpoint-dir)---->containerd->runc(restore 从checkpoint恢复)
                \
                 \create+start(基于原checkpoint恢复到新容器中)----->containerd->runc(restore 从checkpoint恢复)


runc 创建checkpoint目录：

[root@localhost checkpoint3]# pwd
/var/lib/docker/containers/dfeaa8a8f2dd87db70e3bae82c4dae1e36faf10f843c1636e1277faaf2a9230f/checkpoints/checkpoint3
[root@localhost checkpoint3]# ls
cgroup.img        fdinfo-2.img  fs-61.img      ipcns-var-10.img    pagemap-1.img   pstree.img               tmpfs-dev-47.tar.gz.img  tmpfs-dev-51.tar.gz.img
core-1.img        fdinfo-3.img  ids-1.img      mm-1.img            pagemap-61.img  seccomp.img              tmpfs-dev-48.tar.gz.img  utsns-11.img
core-61.img       files.img     ids-61.img     mm-61.img           pages-1.img     tmpfs-dev-43.tar.gz.img  tmpfs-dev-49.tar.gz.img
descriptors.json  fs-1.img      inventory.img  mountpoints-12.img  pages-2.img     tmpfs-dev-46.tar.gz.img  tmpfs-dev-50.tar.gz.img

```

## iSulad 该如何实现

iSulad 默认使用的runtime 为lxc， lxc已经提供了类似runc checkpoint的功能，lxc-checkpoint。尝试直接使用lxc-checkpoint 进行验证。

首先，使用isula 启动一个新的容器

```sh
[root@localhost checkpoint3]# isula run -d --name test --security-opt seccomp=unconfined busybox /bin/sh -c 'i=0; while true; do echo $i; i=$(expr $i + 1); sleep 1; done'
a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5
```

使用lxc-checkpoint 工具对该容器制作checkpoint

```sh
[root@localhost checkpoint3]# lxc-checkpoint -n a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5 -D /tmp/criu/test -P /var/lib/isulad/engines/lcr
[root@localhost checkpoint3]# cd /tmp/criu/test/
[root@localhost test]# ls
cgroup.img    fdinfo-3.img  ids-1.img         iptables-9.img      netns-9.img      pipes-data.img  seccomp.img              tmpfs-dev-71.tar.gz.img
core-135.img  files.img     ifaddr-9.img      mm-135.img          pagemap-135.img  pstree.img      stats-dump               tmpfs-dev-72.tar.gz.img
core-1.img    fs-135.img    inventory.img     mm-1.img            pagemap-1.img    route6-9.img    tmpfs-dev-66.tar.gz.img  tmpfs-dev-73.tar.gz.img
dump.log      fs-1.img      ip6tables-9.img   mountpoints-12.img  pages-1.img      route-9.img     tmpfs-dev-68.tar.gz.img  tty.info
fdinfo-2.img  ids-135.img   ipcns-var-10.img  netdev-9.img        pages-2.img      rule-9.img      tmpfs-dev-69.tar.gz.img  utsns-11.img
```

然后停止容器，尝试使用制作好的checkpoint文件对容器进行恢复，很不幸的是，恢复失败了。

```sh
[root@localhost test]# isula kill a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5
a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5
[root@localhost test]# lxc-checkpoint -r -n a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5 -D /tmp/criu/test -P /var/lib/isulad/engines/lcr
lxc-checkpoint: a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5: criu.c: do_restore: 1086 criu process exited 1, output:

lxc-checkpoint: a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5: criu.c: __criu_restore: 1417 restore process died
lxc-checkpoint: a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5: tools/lxc_checkpoint.c: restore_finalize: 205 Restoring a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5 failed
```

失败的原因是什么呢，可以去/tmp/criu/test目录下查看 restore.log，查看具体失败的原因，可以发现是处理mount信息时失败了，报错找不到该文件。

为什么会找不到该文件呢？很容器联想到，容器前面已经停止了，isulad已经将该容器的根文件系统进行了卸载，是不是因为文件系统找不到引起的呢。

```sh
[root@localhost test]# cat restore.log 
Warn  (criu/log.c:203): The early log isn't empty
Warn  (criu/cr-restore.c:1265): Set CLONE_PARENT | CLONE_NEWPID but it might cause restore problem,because not all kernels support such clone flags combinations!
RTNETLINK answers: File exists
RTNETLINK answers: File exists
     1: Error (criu/mount.c:2298): mnt: Can't mount at /tmp/.criu.mntns.CoHOrT/12-0000000000/etc/hostname: No such file or directory
     1: Error (criu/mount.c:2525): mnt: Unable to statfs /tmp/.criu.mntns.CoHOrT/12-0000000000/etc/hostname: No such file or directory
Error (criu/cr-restore.c:1414): 26996 exited, status=1
Warn  (criu/cr-restore.c:2279): Unable to wait 26996: No child processes
Error (criu/cr-restore.c:2293): Restoring FAILED.
```

我们通过crit命令查看mount.img 中的信息,果然，其中记录了容器根文件系统的信息,容器停止掉，导致容器根文件系统卸载，无法恢复。docker 无该问题是因为docker进行恢复时，均是在start流程中进行的，docker会首先进行容器根文件系统的挂载，然后进行恢复。

```sh
[root@localhost test]# crit show mountpoints-12.img | less
.....
        {
            "fstype": 0, 
            "mnt_id": 349, 
            "root_dev": "253:0", 
            "parent_mnt_id": 318, 
            "flags": "0x200000", 
            "root": "/var/lib/isulad/engines/lcr/a8880ebcd5e4ecb02a3770dc4756980261a7f7369d0c933bb9c8890759d486c5/hostname", 
            "mountpoint": "/etc/hostname", 
            "source": "/dev/mapper/centos-root", 
            "options": "seclabel,attr2,inode64,noquota", 
            "shared_id": 0, 
            "master_id": 0, 
            "sb_flags": "0x0", 
            "ext_key": "etc/hostname"
        },
.....
```

这里有个疑问，docker 可以基于checkpoint文件创建一个新的容器，这里面的配置就存在不一致的情况，docker是如何处理的呢，后面再研究。
为了验证这个猜想，我们使用isulad中的external-rootfs功能，为容器提供一个外部的根文件系统，不跟随容器退出而卸载，是不是就可以了呢，说干就干。

```sh
[root@localhost /]# isula run -d  --external-rootfs=/var/lib/isulad/storage/overlay/8f181e47046ea672f0432ff987b15e64c4a399986e5b04e5feea1b313ca68943/merged --security-opt seccomp=unconfined busybox /bin/sh -c 'i=0; while true; do echo $i; i=$(expr $i + 1); sleep 1; done'
a638b560bdfb580032f54c2041b62e92fc52a909612cd2af3ef837529b4525be
[root@localhost /]# lxc-checkpoint -n a638b560bdfb580032f54c2041b62e92fc52a909612cd2af3ef837529b4525be -D /tmp/criu/test1 -P /var/lib/isulad/engines/lcr
[root@localhost /]# isula kill a63
a63
[root@localhost /]# isula ps -a
CONTAINER ID	IMAGE  	COMMAND               	CREATED           	STATUS                         	PORTS	NAMES                                                           
a638b560bdfb	busybox	"/bin/sh -c 'i=0; ..." 	31 seconds ago    	Exited (137) 2 seconds ago     	     	a638b560bdfb580032f54c2041b62e92fc52a909612cd2af3ef837529b4525be
63db37e2efdc	busybox	"/bin/sh -c 'i=0; ..." 	4 minutes ago     	Exited (137) 3 minutes ago     	     	test1                                                           
a8880ebcd5e4	busybox	"/bin/sh -c 'i=0; ..." 	18 minutes ago    	Exited (137) 15 minutes ago    	     	test                                                            
697196abcc08	busybox	"/bin/sh -c 'i=0; ..." 	About an hour ago 	Up About an hour               	     	lifeng                                                          
8f181e47046e	busybox	"/bin/sh -c 'i=0; ..." 	18 hours ago      	Up About an hour               	     	looper2                                                         
391354ca6a55	busybox	"/bin/sh -c 'i=0; ..." 	About a minute ago	Exited (125) About a minute ago	     	391354ca6a5577ba043fba2210a10bc6a29ebf727d97f76d15bd7a48175754d4
84633c93342c	busybox	"sh"                  	57 seconds ago    	Exited (125) 57 seconds ago    	     	84633c93342c600b34fc885b837e4dcc06a456495a93f5f52a0cfbfc75ade54a
c403f5070bb8	busybox	"/bin/sh -c 'i=0; ..." 	About a minute ago	Exited (125) About a minute ago	     	c403f5070bb8f1da189e6f271e6ab9942bffcbce4ed11140848a707196e36b11
[root@localhost /]# lxc-checkpoint -r -n a638b560bdfb580032f54c2041b62e92fc52a909612cd2af3ef837529b4525be -D /tmp/criu/test1 -P /var/lib/isulad/engines/lcr
[root@localhost /]# systemctl restart isulad
[root@localhost /]# isula ps
CONTAINER ID	IMAGE  	COMMAND               	CREATED           	STATUS          	PORTS	NAMES                                                           
a638b560bdfb	busybox	"/bin/sh -c 'i=0; ..." 	About a minute ago	Up 3 seconds    	     	a638b560bdfb580032f54c2041b62e92fc52a909612cd2af3ef837529b4525be
697196abcc08	busybox	"/bin/sh -c 'i=0; ..." 	About an hour ago 	Up About an hour	     	lifeng                                                          
8f181e47046e	busybox	"/bin/sh -c 'i=0; ..." 	18 hours ago      	Up About an hour	     	looper2   
```
通过以上流程可以看到，基于lxc的CRIU流程是可以验证通过的，iSulad如果要增加这个功能的话，需要添加对应的checkpoint管理功能即可。