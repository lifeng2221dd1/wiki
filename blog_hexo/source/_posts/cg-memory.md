---
title: memory cgroup
date: 2021-01-04 10:23:27
tags: cgroup
---

memory 限制进程内存相关测试笔记

首先创建 1 个测试用的 cgroup 组，将内存使用限制限定在 20M，并且将 memory.memsw.limit_in_bytes 也限定在 20M，即不使用 swap 内存。

1. 向 tmpfs 写入的数据是否计算到本 cgroup 使用的内存中？

验证：使用 dd 命令向 tmpfs 写入数据，查看是否计算。

```bash
cgroup/memory/test » cat memory.usage_in_bytes
3756032
cgroup/memory/test » dd if=/dev/zero of=/tmp/test1 bs=10M count=10
10+0 records in
10+0 records out
104857600 bytes (105 MB, 100 MiB) copied, 0.0933321 s, 1.1 GB/s
cgroup/memory/test » cat memory.usage_in_bytes
10534912
```

向 tmpfs 中写入了 100M 的数据，但是统计信息看，仅仅增加了 10534912 - 3756032 字节的使用。写入到 tmpfs 中的数据是否统计到 cgroup 占用中了呢？如果统计了，明显数据也对不上呀。

为什么呢，这是因为增加的这部分 10534912 - 3756032 字节内存是 cached 缓存数据，实际上写入到 tmpfs 的数据是不会统计到 cgroup 中的。

查看 cgroup 下的 memory.stat 文件,可以看到 cache 占用了较多的内存。

```sh
cgroup/memory/test » cat memory.stat
cache 6148096
rss 278528
rss_huge 0
shmem 0
mapped_file 0
...
```

通过向 memory.force_empty 写入数据， 触发强制回收，可以看到，将内存 cache 回收掉了,usage_in_bytes 统计的信息也降低到正常。

```bash
cgroup/memory/test » echo 0 > memory.force_empty
cgroup/memory/test » cat memory.stat
cache 0
rss 344064
...

cgroup/memory/test » cat memory.usage_in_bytes
4562944
```

当 page cache 较大时，再申请新内存，是会自动回收 pagecache 的内存的。

```sh
cgroup/memory/test » cat memory.usage_in_bytes
14598144 //当前已使用内存14598144，限制大小为20M，再尝试申请10M时，内核会尝试回收cache内存，满足申请需要
cgroup/memory/test » stress --vm 1 --vm-bytes 10M
stress: info: [41520] dispatching hogs: 0 cpu, 0 io, 1 vm, 0 hdd
```

使用 stress 申请 10M 后，内存使用情况

```sh
cgroup/memory/test » cat memory.usage_in_bytes
20815872
cgroup/memory/test » cat memory.limit_in_bytes
20971520
cgroup/memory/test » cat memory.stat
cache 5033984
rss 10600448
rss_huge 0
...
```

结论：向 tmpfs 写入的数据不会计算到本 cgroup 使用的内存中，但是在写入的过程中，内核会使用到 cache 机制，加速执行，这部分内存是会统计到 usage_in_bytes 中的，但是这部分内存是可以自动回收的，在内存使用到限制时，内核会回收该部分内存。

## 参考资料

- [内核 cgroup memory 文档](https://www.kernel.org/doc/Documentation/cgroup-v1/memory.txt)
- [page cache pitfalls](https://engineering.linkedin.com/blog/2016/08/don_t-let-linux-control-groups-uncontrolled)

1. Memory is not reserved for cgroups (as with virtual machines)
2. Page cache usage by apps is counted towards a cgroup’s memory limit, and anonymous memory usage can steal page cache for the same cgroup
3. OS can steal page cache from cgroups if necessary
4. The OS can swap anonymous memory from cgroups if necessary
