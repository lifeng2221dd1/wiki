---
title: 已经退出的thread，进行pthread_detach，是否会回收线程资源
date: 2020-12-28 15:51:46
tags: pthread
---

通过 man pthread_detach，我们知道可以使用 pthread_detach 来设置线程作为 detached 状态，这样在线程推出时，系统可以自动回收线程资源。如果在设置 pthread_detach 设置线程为 detached 前，线程即已经退出了，这样设置 detach 还会生效吗，是否会存在资源泄漏呢。让我们使用 valgrind 工具来实际验证下。

首先，我们编写一个测试程序。

```c
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

void *thread_fn(void *arg)
{
    int n = 1;
    while (n--)
    {
        printf("thread count %d\n", n);
        sleep(1);
    }
    printf("thread exit\n");
    return (void *)1;
}

int main()
{

    pthread_t tid;
    void *retval;
    int err;

    pthread_create(&tid, NULL, thread_fn, NULL);
    getchar();

    pthread_detach(tid);// 设置tid 为detached状态

    return 0;
}
```

在该程序中，我们首先让线程运行结束，先行退出，然后使用 pthread_detach 设置线程为 detached 状态。编译后，使用 valgrind 工具进行测试。

```c
~/wiki/test_code ‹master*› » valgrind --fair-sched=yes --tool=memcheck --leak-check=yes --track-origins=yes ./a.out
==633677== Memcheck, a memory error detector
==633677== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==633677== Using Valgrind-3.16.1 and LibVEX; rerun with -h for copyright info
==633677== Command: ./a.out
==633677==
thread count 0
thread exit

==633677==
==633677== HEAP SUMMARY:
==633677==     in use at exit: 0 bytes in 0 blocks
==633677==   total heap usage: 3 allocs, 3 frees, 2,320 bytes allocated
==633677==
==633677== All heap blocks were freed -- no leaks are possible
==633677==
==633677== For lists of detected and suppressed errors, rerun with: -s
==633677== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```

并无泄漏。

我们修改代码，将`pthread_detach(tid);// 设置tid 为detached状态`注释掉后，再次我们编译后运行。

```c
~/wiki/test_code ‹master*› » valgrind --fair-sched=yes --tool=memcheck --leak-check=yes --track-origins=yes ./a.out
==634098== Memcheck, a memory error detector
==634098== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==634098== Using Valgrind-3.16.1 and LibVEX; rerun with -h for copyright info
==634098== Command: ./a.out
==634098==
thread count 0
thread exit

==634098==
==634098== HEAP SUMMARY:
==634098==     in use at exit: 272 bytes in 1 blocks
==634098==   total heap usage: 3 allocs, 2 frees, 2,320 bytes allocated
==634098==
==634098== 272 bytes in 1 blocks are possibly lost in loss record 1 of 1
==634098==    at 0x483DB0A: calloc (vg_replace_malloc.c:760)
==634098==    by 0x40149CA: allocate_dtv (dl-tls.c:286)
==634098==    by 0x40149CA: _dl_allocate_tls (dl-tls.c:532)
==634098==    by 0x486E322: allocate_stack (allocatestack.c:622)
==634098==    by 0x486E322: pthread_create@@GLIBC_2.2.5 (pthread_create.c:660)
==634098==    by 0x109279: main (in /home/lifeng/wiki/test_code/a.out)
==634098==
==634098== LEAK SUMMARY:
==634098==    definitely lost: 0 bytes in 0 blocks
==634098==    indirectly lost: 0 bytes in 0 blocks
==634098==      possibly lost: 272 bytes in 1 blocks
==634098==    still reachable: 0 bytes in 0 blocks
==634098==         suppressed: 0 bytes in 0 blocks
==634098==
==634098== For lists of detected and suppressed errors, rerun with: -s
==634098== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

检测到了线程资源 allocate_stack 泄漏。

通过以上验证，我们可以得到结论：如果在调用 pthread_detach 线程为 detached 前，线程即已经退出的情况下，pthread_detach 也会生效，系统也会自动回收线程申请的资源。
