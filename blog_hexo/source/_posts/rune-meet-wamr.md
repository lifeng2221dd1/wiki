---
title: rune meet wamr
date: 2021-06-17 15:20:52
tags: enclave
---

# rune meet WAMR

记录使用WAMR作为enclave runtime 配合rune启动机密计算容器。

## 搭建WAMR-sgx 环境，Build WAMR vmcore (iwasm) for Linux SGX

具体步骤官方文档[build WAMR vmcore](https://github.com/bytecodealliance/wasm-micro-runtime/blob/main/doc/linux_sgx.md#build-wamr-vmcore-iwasm-for-linux-sgx)

```bash
# source /opt/intel/sgxsdk/environment
# cd product-mini/platforms/linux-sgx/
# mkdir build
# cd build
# cmake ..
# make
```

执行成功后，会编译生成两个二进制文件

* libvmlib.a for Enclave part
* libvmlib_untrusted.a for App part

接一下来编译SGX的实例程序

```bash
# source /opt/intel/sgxsdk/environment
# cd enclave-sample
# make SGX_MODE=HW
GEN  =>  App/Enclave_u.c
CC   <=  App/Enclave_u.c
CXX  <=  App/App.cpp
CP libvmlib_untrusted.a  <=  ../build/libvmlib_untrusted.a
LINK =>  iwasm
GEN  =>  Enclave/Enclave_t.c
CC   <=  Enclave/Enclave_t.c
CXX  <=  Enclave/Enclave.cpp
CP libvmlib.a  <=  ../build/libvmlib.a
LINK =>  enclave.so
The project has been built in release hardware mode.
Please sign the enclave.so first with your signing key before you run the iwasm to launch and access the enclave.
To sign the enclave use the command:
   /opt/intel/sgxsdk/bin/x64/sgx_sign sign -key <your key> -enclave enclave.so -out <enclave.signed.so> -config Enclave/Enclave.config.xml
You can also sign the enclave using an external signing tool. See User's Guide for more details.
To build the project in simulation mode set SGX_MODE=SIM. To build the project in prerelease mode set SGX_PRERELEASE=1 and SGX_MODE=HW.
```

调用sgx sdk工具对enclave.so 进行签名

```bash
# /opt/intel/sgxsdk/bin/x64/sgx_sign sign -key ./Enclave/Enclave_private.pem -enclave enclave.so -out enclave.signed.so -config Enclave/Enclave.config.xml

<!-- Please refer to User's Guide for the explanation of each field -->
<EnclaveConfiguration>
    <ProdID>0</ProdID>
    <ISVSVN>0</ISVSVN>
    <StackMaxSize>0x100000</StackMaxSize>
    <HeapMaxSize>0x2000000</HeapMaxSize>
    <ReservedMemMaxSize>0x1000000</ReservedMemMaxSize>
    <ReservedMemExecutable>1</ReservedMemExecutable>
    <TCSNum>10</TCSNum>
    <TCSPolicy>1</TCSPolicy>
    <DisableDebug>0</DisableDebug>
    <MiscSelect>0</MiscSelect>
    <MiscMask>0xFFFFFFFF</MiscMask>
</EnclaveConfiguration>
tcs_num 10, tcs_max_num 10, tcs_min_pool 1
The required memory is 72036352B.
The required memory is 0x44b3000, 70348 KB.
Succeed.
```

编译完成后，会生成以下几个二进制文件：

```c
-rw-rw-r-- 1 lifeng lifeng 604984 6月  17 15:37 enclave.signed.so  ====》签名后的enclave文件，其中链接了WAMR所需要的libvmlib.a，用于创建enclave实例，在enclave中运行WAMR runtime
-rwxrwxr-x 1 lifeng lifeng 604984 6月  17 15:33 enclave.so ===》未签名前的文件
-rwxrwxr-x 1 lifeng lifeng  55976 6月  17 15:33 iwasm   ===》可执行文件，可用于调试执行wasm文件
-rw-rw-r-- 1 lifeng lifeng 865140 6月  17 15:33 libvmlib.a ===》从product-mini/platforms/linux-sgx/拷贝出的静态库
-rw-rw-r-- 1 lifeng lifeng  19714 6月  17 15:33 libvmlib_untrusted.a ===》从product-mini/platforms/linux-sgx/拷贝出的静态库
```

## 准备测试程序，编译成为wasm格式

这里准备一个简单的实例程序，用于在enclave中的WAMR runtime中运行。

```c
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    char *buf;

    printf("Hello world!\n");

    buf = malloc(1024);
    if (!buf) {
        printf("malloc buf failed\n");
        return -1;
    }

    printf("buf ptr: %p\n", buf);

    sprintf(buf, "%s", "1234\n");
    printf("buf: %s", buf);

    while (1) {
        sleep(1);
    }

    free(buf);
    return 0;
}
```

首先安装wasi sdk

```bash
# wget https://github.com/WebAssembly/wasi-sdk/releases/download/wasi-sdk-12/wasi-sdk-12.0-linux.tar.gz
# tar -xzvf wasi-sdk-12.0-linux.tar.gz -C /opt/wasi-sdk
```

使用wasi sdk 编译测试程序：

```bash
# /opt/wasi-sdk/bin/clang -O3 -o test.wasm test.c
```

安装 wamrc 用于将.wasm文件转换为.aot文件,可以参考[wamrc](https://github.com/bytecodealliance/wasm-micro-runtime#build-wamrc-aot-compiler)

```bash
# cd wamr-compiler
# ./build_llvm.sh (or "./build_llvm_xtensa.sh" to support xtensa target)
# mkdir build && cd build
# cmake ..
# make
# sudo cp ./wamrc /usr/local/bin
```

将.wasm文件转换为.aot,最终得到test.aot文件

```bash
# wamrc  -sgx -o test.aot test.wasm
```


## 使用iwasm 进行验证

```bash
# ./iwasm test.aot

Hello world!
buf ptr: 0x11500
buf: 1234

```

iwasm 进行运行的流程总结如下：
<img src="iwasm-sgx-Page-1.png" alt="iwasm" style="zoom:100%;" />


## 与rune的结合

rune提供了一层PAL层接口，用于rune调用enclave runtime的能力去创建enclave、运行进程等。

当前WAMR在目录wasm-micro-runtime/product-mini/platforms/linux-sgx/enclave-sample中的APP.cpp中已经提供了对应的封装接口

```c
int pal_get_version(void) __attribute__((weak, alias ("wamr_pal_get_version")));

int pal_init(const struct wamr_pal_attr *attr)\
__attribute__ ((weak, alias ("wamr_pal_init")));

int pal_create_process(struct wamr_pal_create_process_args *args)\
__attribute__ ((weak, alias ("wamr_pal_create_process")));

int pal_exec(struct wamr_pal_exec_args *args)\
__attribute__ ((weak, alias ("wamr_pal_exec")));

int pal_kill(int pid, int sig) __attribute__ ((weak, alias ("wamr_pal_kill")));

int pal_destroy(void) __attribute__ ((weak, alias ("wamr_pal_destroy")));
```

PAL层可以编译为so供rune dlopen打开使用，编译步骤：

```bash
# g++ -shared -fPIC -o libwamr-pal.so App/*.o libvmlib_untrusted.a -L/opt/intel/sgxsdk/lib64 -lsgx_urts -lpthread -lssl -lcrypto
# cp ./libwamr-pal.so /usr/lib/libwamr-pal.so
```

rune运行的流程总结如下：
<img src="rune-sgx-Page-1.png" alt="rune" style="zoom:100%;" />

