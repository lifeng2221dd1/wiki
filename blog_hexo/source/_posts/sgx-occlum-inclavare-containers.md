---
title: sgx_occlum_inclavare_containers
date: 2021-03-10 15:21:55
tags: enclave
---


## intel SGX

Intel推出SGX(software guard extensions)指令集扩展, 旨在以硬件安全为强制性保障, 不依赖于固件和软件的安全状态, 提供用户空间的可信执行环境, 通过一组新的指令集扩展与访问控制机制, 实现不同程序间的隔离运行, 保障用户关键代码和数据的机密性与完整性不受恶意软件的破坏.

官方 SGX 的设计预想如下，从官方资料来看，使用SGX技术是针对单个应用级别的。也就是说开发人员如果要使用SGX技术的话，需要识别出哪些数据结构、计算过程属于安全级别，放入enclave中执行。对开发人员要求较高，另外既有的软件如果想采用SGX技术的话，面临很大的拆分工作量。

<img src="sgx_application.png" alt="sgx" style="zoom:100%;" />


### graphene

graphene 采用了libOS的思想，将业务进程整体迁移进入SGX中执行，不必关心拆分应用到安全空间，降低SGX的开发门槛。

同样的，在host上如果使用graphene可以进行业务进程的整体迁移的话，容器化就比较方便了。也就是将graphene所需的依赖库、文件、配置，整体放入容器镜像。然后就可以用容器镜像启动基于SGX的容器。

#### 镜像制作

搭建graphene后，按照官方说明，很容易的就可以制作1个使能graphene功能的容器镜像，并且基于这个镜像启动1个采用SGX enclave技术的容器，具体可以参考 [gsc](https://graphene.readthedocs.io/en/latest/manpages/gsc.html)。

整体思路：
```c
step 1:compile graphene base image
                                                |
                                                |
config.yaml                                     |
+                                               | ====> graphene image
Dockerfile.ubuntu18.04.compile.template         |
                                                |
                                                |


step 2:build unsigned graphened image
                                                |
                                                |
graphene image                                  |
+                                               | ====> gsc-<image-name>-unsigned
Dockerfile.ubuntu18.04.build.template           |
+                                               |
normal Docker image <image-name>                |                            


step 3:sign graphened image
                                                |
                                                |
gsc-<image-name>-unsigned                       |
+                                               | ====> gsc-<image-name>
Dockerfile.ubuntu18.04.sign.template            |
+                                               |
enclave-key.pem                                 |    

```


镜像制作具体的步骤分以下三步：

从1个普通的<image-name>镜像制作成为符合graphene的镜像gsc-<image-name>，主要分3步。gsc build-graphene 用来执行第一步骤。gsc build 用于执行前两步，最后 gsc sign-image用于执行最后的签名步骤。

1. 编译 Graphene。 第一步骤根据config.yaml的配置（使用系统的发行版本、graphene的版本、SGX的版本）从源码中编译安装graphene。config.yaml的配置示例如下：

```c
# The only currently supported distro is Ubuntu 18.04; to add another distro, you must add three new
# Dockerfiles (compile, build, sign) under templates/
Distro: "ubuntu18.04"

# If you're using your own fork and branch of Graphene, specify the GitHub link and the branch name
# below; typically, you want to keep the default values though
Graphene:
    Repository: "https://github.com/oscarlab/graphene.git"
    Branch:     "master"

# Specify the Intel SGX driver installed on your machine (more specifically, on the machine where
# the graphenized Docker container will run); there are several variants of the SGX driver:
#
#   - legacy out-of-tree driver: use the defaults below, but adjust the branch name
#
#   - DCAP out-of-tree driver: use something like the below values
#         Repository: "https://github.com/intel/SGXDataCenterAttestationPrimitives.git"
#         Branch:     "DCAP_1.6 && cp -r driver/linux/* ."
#
#   - DCAP in-kernel driver: use empty values like below
#         Repository: ""
#         Branch:     ""
#
SGXDriver:
    Repository: "https://github.com/01org/linux-sgx-driver.git"
    Branch:     "sgx_driver_1.9"
```

其中描述了编译graphene image 所需的基础发行版本信息，以及Graphene、SGX driver的版本信息。

gsc 会调用Dockerfile.ubuntu18.04.compile.template 中镜像制作步骤，进行Graphene的编译,其中关键的编译步骤如下：

```c
...
RUN git clone {{Graphene.Repository}} /graphene

RUN cd /graphene \
    && git fetch origin {{Graphene.Branch}} \
    && git checkout {{Graphene.Branch}}

{% if SGXDriver.Repository %}
RUN cd /graphene/Pal/src/host/Linux-SGX \
    && git clone {{SGXDriver.Repository}} linux-sgx-driver \
    && cd linux-sgx-driver \
    && git checkout {{SGXDriver.Branch}}
ENV ISGX_DRIVER_PATH "/graphene/Pal/src/host/Linux-SGX/linux-sgx-driver"
{% else %}
ENV ISGX_DRIVER_PATH ""
{% endif %}

RUN cd /graphene \
    && make -s -j WERROR=1 SGX=1 {% if debug %} DEBUG=1 {% endif %}

{% if linux %}
RUN cd /graphene \
    && make -s -j WERROR=1 {% if debug %} DEBUG=1 {% endif %}
{% endif %}

# Translate runtime symlinks to files
RUN for f in $(find /graphene/Runtime -type l); do cp --remove-destination $(realpath $f) $f; done
```

2. 从普通镜像转换为Graphene镜像。这一步骤首先基于基础的普通镜像，然后从第一步拷贝需要的Graphene 编译后的二进制到本镜像内。然后准备镜像配置信息，比如可执行二进制的路径，库的路径，扫描整个镜像的文件形成可信的文件列表。GSC忽略了以/boot, /dev, /proc, /var, /sys and /etc/rc开头的文件。GSC将这些信息以及可信文件列表形成一个新的manifest文件。最后一步，将镜像的执行入口entrypoint修改为apploader.sh脚本。这个脚本会生成Intel SGX token 并且启动pal-Linux-SGX loader。注意这里生成的镜像（gsc-<image-name>-unsigned）不能成功地启动Intel SGX enclave，由于一些关键文件以及仍未进行envlace的签名。

其中关键步骤如下，会从步骤1中生成的graphene image中拷贝graphene 运行所需的文件。

```c
# Include previously-prepared Docker image with Graphene (if any) or compile Graphene from sources
{% if Graphene.Image %}
FROM gsc-{{Graphene.Image}} AS graphene
{% else %}
{% include "Dockerfile.ubuntu18.04.compile.template" %}
{% endif %}

# Combine Graphene image with the original app image
FROM {{app_image}}

.......

# Copy Graphene runtime and signer tools to /graphene
RUN mkdir -p /graphene \
    && mkdir -p /graphene/Pal/src \
    && mkdir -p /graphene/Runtime \
    && mkdir -p /graphene/Scripts \
    && mkdir -p /graphene/Tools \
    && mkdir -p /graphene/python
COPY --from=graphene /graphene/Pal/src/host/Linux-SGX/generated_offsets.py /graphene/python/
COPY --from=graphene /graphene/Runtime/ /graphene/Runtime
COPY --from=graphene /graphene/Scripts/Makefile.configs /graphene/Scripts
COPY --from=graphene /graphene/Scripts/Makefile.Host /graphene/Scripts
COPY --from=graphene /graphene/Tools/argv_serializer /graphene/Tools
COPY --from=graphene /graphene/python /graphene/python

{% if debug %}
COPY --from=graphene /graphene/Pal/gdb_integration/debug_map_gdb.py \
                     /graphene/Pal/gdb_integration/
COPY --from=graphene /graphene/Pal/gdb_integration/pagination_gdb.py \
                     /graphene/Pal/gdb_integration/
COPY --from=graphene /graphene/Pal/gdb_integration/graphene.gdb \
                     /graphene/Pal/gdb_integration/

COPY --from=graphene /graphene/Pal/src/host/Linux/gdb_integration/graphene_linux_gdb.py \
                     /graphene/Pal/src/host/Linux/gdb_integration/
RUN ln -sf /graphene/Pal/gdb_integration /graphene/Pal/src/host/Linux/gdb_integration/common

COPY --from=graphene /graphene/Pal/src/host/Linux-SGX/gdb_integration/sgx_gdb.so \
                     /graphene/Pal/src/host/Linux-SGX/gdb_integration/
COPY --from=graphene /graphene/Pal/src/host/Linux-SGX/gdb_integration/graphene_sgx_gdb.py \
                     /graphene/Pal/src/host/Linux-SGX/gdb_integration/
COPY --from=graphene /graphene/Pal/src/host/Linux-SGX/gdb_integration/graphene_sgx.gdb \
                     /graphene/Pal/src/host/Linux-SGX/gdb_integration/
RUN ln -sf /graphene/Pal/gdb_integration /graphene/Pal/src/host/Linux-SGX/gdb_integration/common
{% endif %}

# Copy helper scripts and Graphene manifest
COPY *.py /
COPY apploader.sh /
COPY entrypoint.manifest /

# For convenience (e.g. for debugging), create a link to pal_loader in the root dir
RUN ln -sf /graphene/Runtime/pal_loader

# Generate trusted arguments if required
{% if not insecure_args %}
RUN /graphene/Tools/argv_serializer {{binary}} {{binary_arguments}} "{{"\" \"".join(cmd)}}" > /trusted_argv
{% endif %}

# Docker entrypoint/cmd typically contains only the basename of the executable so create a symlink
RUN cd / \
    && which {{binary}} | xargs ln -s || true

# Mark apploader.sh executable, finalize manifest, and remove intermediate scripts
RUN chmod u+x /apploader.sh \
    && python3 -B /finalize_manifest.py \
    && rm -f /finalize_manifest.py

# Define default command
ENTRYPOINT ["/bin/bash", "/apploader.sh"]
CMD [{% if insecure_args %} "{{'", "'.join(cmd)}}" {% endif %}]
```
   
3. 对Intel SGX enclave进行签名。第三步使用Graphene的签名工具为SGX enclave生成SIGSTRUCT文件，同时也生成了SGX-specific manifest文件。签名所需要的签名key，是通过sign-image工具传入的，并在镜像制作环节使用。最后生成的镜像gsc-<image-name>包含了所有用来启动sgx enclave 所需要的文件
```c
    ./gsc sign-image python enclave-key.pem
```

关键步骤如下， 在镜像中调用 /graphene/python/graphene-sgx-sign 工具进行签名：

```c
# Sign image in a separate stage to ensure that signing key is never part of the final image

FROM {{image}} as unsigned_image

RUN locale-gen en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

COPY gsc-signer-key.pem /gsc-signer-key.pem

RUN /graphene/python/graphene-sgx-sign \
        -libpal /graphene/Runtime/libpal-Linux-SGX.so \
        -key /gsc-signer-key.pem \
        -manifest /entrypoint.manifest \
        -output /entrypoint.manifest.sgx

# This trick removes all temporary files from the previous commands (including gsc-signer-key.pem)
FROM {{image}}

COPY --from=unsigned_image /*.sig /
COPY --from=unsigned_image /*.sgx /

RUN rm /graphene/python/graphene-sgx-sign /graphene/python/graphenelibos/sgx_sign.py
```

#### 基于制作好的镜像，运行graphene容器

前面镜像制作步骤完成后，即可查询到制作好的镜像

```c
~/openeuler_iSulad ‹master*› » sudo docker images                                                                                                               130 ↵
REPOSITORY                                                        TAG                 IMAGE ID            CREATED             SIZE
gsc-python                                                        latest              66cde5e4fa81        5 days ago          1.28GB
```

查询该镜像的entrypoint 入口，可以看到, 镜像中会去执行apploader.sh脚本。

```c
....
            "Entrypoint": [
                "/bin/bash",
                "/apploader.sh"
            ],
.....
```

apploader.sh脚本中的内容是什么呢？可以通过该镜像启动一个bash容器，查看其中的内容。
```c
root@b10a27b49872:/# cat apploader.sh 
#!/usr/bin/env bash

set -ex

# Set default PAL to Linux-SGX
if [ -z "$GSC_PAL" ] || [ "$GSC_PAL" == "Linux-SGX" ]
then
    GSC_PAL=Linux-SGX
    /graphene/python/graphene-sgx-get-token -output /entrypoint.token -sig /entrypoint.sig
    /graphene/Runtime/pal-$GSC_PAL /graphene/Runtime/libpal-$GSC_PAL.so init /entrypoint  "${@}"
else
    /graphene/Runtime/pal-$GSC_PAL /graphene/Runtime/libpal-$GSC_PAL.so init /entrypoint  "${@}"
```

从脚本内容看，实际调用graphene提供的操作enclave的能力。

运行一个graphene容器，只需要执行

```c
# isula run --device=/dev/isgx -v /home/lifeng/.dockerenv:/.dockerenv -v /home/lifeng/test.py:/test.py -v /var/run/aesmd/aesm.socket:/var/run/aesmd/aesm.socket -it  gsc-python -c 'printf("hello world!")'

+ /graphene/python/graphene-sgx-get-token -output /entrypoint.token -sig /entrypoint.sig
Attributes:
mr_enclave: 3b46eada7c8512026215437371134240f2e5c7921a60f2fc7366b08e2099358d
mr_signer: a9ccb497ff6865f04f9d0a117e59f9ce5a4439a4b7ef439551d52501f667653a
isv_prod_id: 0
isv_svn: 0
attr.flags: 0600000000000000
attr.xfrm: 1f00000000000000
misc_select: 00000000
misc_mask: 00000000
modulus: 0d22e6c866d5390be73e23985ebf9266...
exponent: 3
signature: d1bc0ed2ee06eabbb5a39696bb14820e...
date: 2021-03-15
+ /graphene/Runtime/pal-Linux-SGX /graphene/Runtime/libpal-Linux-SGX.so init /entrypoint -c 'print("HelloWorld!")'
........
hello world!
```
从执行过程中可以看到，其实entrypoint执行了appload.sh中的两个步骤，我们也可以直接使用容器镜像，启动bash后，手动在容器中执行以下命令，也可以启动enclave 容器
```c
    GSC_PAL=Linux-SGX
    /graphene/python/graphene-sgx-get-token -output /entrypoint.token -sig /entrypoint.sig
    /graphene/Runtime/pal-Linux-SGX /graphene/Runtime/libpal-Linux-SGX.so init /entrypoint -c 'print("HelloWorld!")'
```

#### graphene总结
Graphene 实际的执行流程，可以整体执行流程，可以通过下图进行下理解。
```c

UnModified APP Image ---
                        \                               sign the image
                         |-> unsigned trusted image -------------------->signed trusted image -----> image repository
                        /                                                                         
Graphene ---------------  

develop Environment
---------------------------------------------------------------------------------
deploy Environment
                    pulled from                     run with default entrypoint
image repository ----------------->docker\isulad --------------------------------> enclave app
```

### 阿里的enclave container 项目

思路与graphene类似, 区别在于使用rust语言编写了libos occlum以及为了方便管理，增加了rune及其容器中执行的init-runelet。

<img src="enclave-container.png" alt="enclave-container" style="zoom:100%;" />

其中Enclave容器的1号进程被称为init-runelet，它通过PAL API来管理Enclave，也就是说，创建容器、启动容器、在容器中执行exec、kill容器等需要操作enclave 环境的动作，都会由容器引擎通过rune操作。也就是将graphene中通过 /graphene/Runtime/pal-Linux-SGX 执行的步骤进行了拆分，对外暴露。

enclave container调用流程如下：
容器引擎---->rune--->init-runelet-------（PAL-API）------->enclave.

#### enclave container总结
Graphene 实际的执行流程，可以整体执行流程，可以通过下图进行下理解。
```c

UnModified APP Image ---
                        \                               sign the image
                         |-> unsigned trusted image -------------------->signed trusted image -----> image repository
                        /                                                                         
occlum  ---------------  

develop Environment
---------------------------------------------------------------------------------
deploy Environment                                                              |  enclave container     |
                    pulled from                   run with rune                 |------------------------|
image repository ----------------->docker\isulad --------------->rune---------->|init-runelet            |
                                                                                |    |                   |
                                                                                |    | call with PAL-API |
                                                                                |    | (init\create..)   |   
                                                                                |    |                   |
                                                                                |------------------------|
                                                                                |      enclave           |
                                                                                |                        |
                                                                                |------------------------|
```     

## Arm TrustZone

### trustzone简介

ARM从ARM v6版本引入trustzone技术。TrustZone技术将CPU划分为NWS（Normal World Status）和 SWS（Secure World Status）。

- TEE（Trust Execution Environment），可信执行环境
- REE （Rich Execution Environment），通用执行环境
- GP （Global Platform）TEE标准制定者，描述TA、CA通信的标准以及API
- TA（Trusted APP），可信应用，在TEE中执行的应用
- CA（Client APP），客户端应用，在REE中执行，作为与TA通信的客户端
- Trusted OS：在TEE中执行的OS，比如OP-tee、Trusty、鸿蒙iTrustee等。通常都有独立的二进制，由设备厂商提供，与硬件设计强相关，无法由第三方更新。对外接口符合GP标准。
- NWS（Normal World Status）、SWS（Secure World Status）。两者通过ARM的指令进行切换


<img src="trustzone.png" alt="trustzone" style="zoom:100%;" />

Intel SGX 技术是针对应用粒度的，不同的应用可以开发不同的enclave。也就是说，在同一台host机器上，可以同时存在很多个SGX enclave。
TrustZone 与SGX不同，TEE的功能，依赖于安全OS，首先要确保设备上已经部署了安全OS，并且要确保其是可信的。在同一个host上，只有一个secure OS。

对于1个应用来说，如果有需要可信保护的处理过程，则需要实现单独的可信应用（TA），在REE中的APP以及在TEE中的TA需要分别开发，物理上他们是两个不同的程序。

具体流程如下：

1. 在REE中运行的APP执行到安全处理部分时，它通过TEE标准接口向TEE中运行的TA发消息
2. REE进入内核态，通过驱动将消息送到TEE侧；
3. TEE中的TA收到消息后，来执行安全计算
4. 最后将结果返回到REE中的APP，但过程数据仍然在TEE侧，不会泄漏到REE。

TrustZone 利用的是CPU时间片切换来模拟了安全世界，这两个世界可以将它理解成一个CPU上处理的两个进程，它们通过上下文切换来将CPU的时间片占满以利用CPU。从安全角度，仅仅分时复用或拟化，是不足以确保安全的，因此ARM另外定义了安全框架，从硬件级别两个世界，包括Timer、TRNG、TZPC、MMU、Cache等相关设备，不同的芯片厂商会有自己的考虑，这个设备可能是双份的，或者是动态切换以达到隔离目的。同时，安全侧也需要有一个可信操作系统执行应用。从原理上，REE侧和TEE侧是对等的，因此并不会性能的差异。应用程序的开发，除了使用TEE定义的标准接口，依赖的都POSIX API，使用标准的开发语言。
在部署应用时，SGX只需要在代码中定义即可，而TrustZone中则需要单独开发部署TA。


### 参考文档

- [intel sgx](https://download.01.org/intel-sgx/sgx-linux/2.13/docs/)
- [occlum](https://blog.csdn.net/SOFAStack/article/details/108138440)
- [graphene](https://github.com/oscarlab/graphene)
- [SGX vs TrustZone](https://aijishu.com/a/1060000000150905)
- [Tz-Container](https://engine.scichina.com/publisher/scp/journal/SCIS/doi/10.1007/s11432-019-2707-6?slug=fulltext)
- [enclave container](https://blog.csdn.net/openanolis/article/details/109747031)