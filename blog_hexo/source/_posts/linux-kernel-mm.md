---
title: linux kernel mm
date: 2021-07-14 09:12:09
tags: kernel
---

学习linux kernel 内存管理知识随笔

## 内核空间

1. 页的概念：内核把物理页作为内存管理的基本单元，内核使用struct page 描述系统中的每个物理页

2. 区的概念： 由于硬件限制，内核并不能对所有的页一视同仁，有些页位于内存中特定的物理地址，所以不能将其用于一些特定的任务，由于存在这种限制，所以内核将页划分为不同的区。主要硬件限制如下：
    a. 一些硬件只能用某些特定的地址进行DMA
    b. 一些体系结构的物理内存寻址范围比虚拟寻址范围大，如32位系统，这样的话，一些物理内存就不能永久地映射到内核空间

3. 32 位linux 系统，进程的4G虚拟内存地址空间被分为用户空间（0-3G）和内核空间（3G-4G）。内核空间由内核负责映射，有自己对应的页表，用户进程各自有各自不同的页表。
4. 逻辑地址--线性地址--物理地址，MMU负责逻辑地址通过分段单元硬件电路解析为线性地址（虚拟地址），线性地址通过分页单元硬件电路解析解析为实际的物理地址，linux采用的分段如下，从中可以看到，所采用的分段起始地址均为0x00000, 因此在linux中逻辑地址与线性地址一致。

```
Segment     Base        G   Limit     S     Type    DPL D/B P
user code   0x00000000  1   0xfffff   1     10      3   1   1
user data   0x00000000  1   0xfffff   1     2       3   1   1
kernel code 0x00000000  1   0xfffff   1     10      0   1   1
kernel data 0x00000000  1   0xfffff   1     2       0   1   1
```

5. 内核申请内存时，__get_free_page()、 __get_free_pages()或者kmalloc()不能指定ZONE_HIGHMEM,因为这些函数返回的都是逻辑地址，而不是page结构，这两个函数分配的内存当前可能还没有映射到内核的虚拟地址空间，因此，也可能没有逻辑地址。
   
6.  vmalloc 的工作方式类似于kmalloc，只不过前者分配的内存虚拟地址是连续的，而物理地址则无需连续。这也是用户空间分配函数的工作方式：由malloc返回的页在进程的虚拟地址空间是连续的，但是并不能保证他们在物理RAM上是连续的。kmalloc函数确保页在物理地址上是连续的，vmalloc函数只确保虚拟地址空间内是连续的，通过分配非连续的物理内存块，再修正内核页表，将内存映射到逻辑地址空间的连续区域。vmalloc 将物理上不连续的页转换为虚拟地址空间上连续的页，必须建立页表项。

7.  slab分配器在内核中扮演通用数据结构缓存层的角色，slab分配器设计基本原则
    a. 频繁使用的数据结构也会频繁的分配与释放，因此应当缓存他们
    b. 频繁的分配和回收必然会导致内存碎片，使用空闲链表进行对象的缓存，已释放的数据结构并不是释放，而是放回空闲链表
    c. 回收的对象可以立即投入到下一次的分配，空闲链表可以提高性能
    d. 如果让部分缓存属于单个的处理器，那么分配和释放就可以在不加SMP锁的情况下进行
    e. 如果分配器是与NUMA相关的，就可以从相同的内存节点为请求者进行分配

8.  slab层把不同的对象划分为所谓的高速缓存组，每个高速缓存组存放不同类型的对象。每种对象类型对应一个高速缓存。kmalloc接口建立在slab层之上，使用了一组通用高速缓存。

9. 页表转换， Linux 内核采用4级页表方式进行虚拟地址到物理地址的转换

Page Global Directory
Page Upper Directory
Page Middle Directory
Page Table

页表转换示意图：

<img src="page_table.png" alt="page_table" style="zoom:100%;" />

每个进程都有自己的mm struct 结构体，在mm_struct->pgd指向pgd页表数组，大小一般为2页 page大小。由于pgd_t、pud_t、pmd、 pte 均为8字节大小，因此pgd最多可寻址1024个pgd_t表项（8K/8）。

在进程切换时，会将mm_struct->pgd指向的物理地址赋值到cr3寄存器，然后根据虚拟地址的PGD 的index获取到实际的pgd_t 表项。

比如从虚拟地址转换为物理地址的实例代码如下：

```
static pte_t *kvm_mips_walk_pgd(pgd_t *pgd, struct kvm_mmu_memory_cache *cache,
				unsigned long addr)
{
	p4d_t *p4d;
	pud_t *pud;
	pmd_t *pmd;

	pgd += pgd_index(addr); // 从addr 获取index，然后加上基础的pgd，获取addr对应的pgd_t表项的虚拟地址
	if (pgd_none(*pgd)) {
		/* Not used on MIPS yet */
		BUG();
		return NULL;
	}
	p4d = p4d_offset(pgd, addr);// pgd中下级页表的物理地址的虚拟地址 + 从addr中获取的p4d的index，得到指向p4d_t表项的虚拟地址
	pud = pud_offset(p4d, addr);// p4d中下级页表的物理地址的虚拟地址 + 从addr中获取的pud的index，得到指向pud_t表项的虚拟地址
	if (pud_none(*pud)) {
		pmd_t *new_pmd;

		if (!cache)
			return NULL;
		new_pmd = kvm_mmu_memory_cache_alloc(cache);
		pmd_init((unsigned long)new_pmd,
			 (unsigned long)invalid_pte_table);
		pud_populate(NULL, pud, new_pmd);
	}
	pmd = pmd_offset(pud, addr);// pud中下级页表的物理地址的虚拟地址 + 从addr中获取的pmd的index，得到指向pmd_t表项的虚拟地址
	if (pmd_none(*pmd)) {
		pte_t *new_pte;

		if (!cache)
			return NULL;
		new_pte = kvm_mmu_memory_cache_alloc(cache);
		clear_page(new_pte);
		pmd_populate_kernel(NULL, pmd, new_pte);
	}
	return pte_offset_kernel(pmd, addr);// pmd中下级页表的物理地址的虚拟地址 + 从addr中获取的pte的index，得到指向pte_t表项的虚拟地址
}

```

## 用户空间

1. 一个进程的虚拟地址空间主要由两个数据结构来描述，mm_struct与vm_area_struct，每个进程有一个mm_struct结构，在进程的task_struct结构体中有一个指针指向mm_struct，mm_struct是对整个进程的用户空间的描述，而进程的虚拟空间可能有多个虚拟区间(这里的区间就是由vm_area_struct来描述). vm_area_struct是描述进程虚拟空间的基本单元，那这些基本单元又是如何管理组织的呢？内核采取两种方式来组织这些基本单元，第一，正如mm_struct中的mmap指针指向vm_area_struct，以链表形式存储，这种结构主要用来遍历节点；第二，以红黑树来组织vm_area_struct，这种结构主要在定位特定内存区域时用来搜索，以降低耗时。

2. 进程只能访问有效内存区域内的内存地址，每个内存区域也具有相关权限如对相关进程有可读、可写、可执行的属性，内存区域可以包含各种内存对象，比如：
    a. 可执行文件代码的内存映射，称为代码段
    b. 可执行文件的已初始化全局变量的内存映射，成为数据段
    c. 包含未初始化全局变量，也就是bss段的零页的内存映射
    d. 用于进程用户空间栈的内存映射（进程的内核栈独立存在并且由内核维护）
    e. 每一个c库或者动态链接库的代码段、数据段等
    f. 任何内存映射文件
    g. 任何共享内存段
    h. 任何匿名的内存映射，比如malloc分配的内存

3. 虚拟内存区域（VMA），由vm_area_struct结构体描述了指定地址空间内连续区间上的一个独立内存范围。内核将每个内存区域作为一个单独的内存对象管理，每个内存区域拥有一致的属性，比如访问权限等。没一个VMA就可以代表不同类型的内存区域（比如内存映射文件、用户空间栈等），可以通过查看/proc文件系统或者pmap 查看每个进程的虚拟内存区域分布

```
18008:   /opt/google/chrome/chrome --type=renderer --field-trial-handle=15406341687504510376,2460755200897945042,131072 --lang=en-US --origin-trial-disabled-features=SecurePaymentConfirmation --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=11 --no-v8-untrusted-code-mitigations --shared-files=v8_context_snapshot_data:100
00000278ab4c0000      4K -----   [ anon ]
00000278ab4c1000    120K rw---   [ anon ]
00000278ab4df000      8K -----   [ anon ]
00000278ab4e1000    120K rw---   [ anon ]
00000278ab4ff000      8K -----   [ anon ]
00000278ab501000    120K rw---   [ anon ]
00000278ab51f000      8K -----   [ anon ]
00000278ab521000    120K rw---   [ anon ]
00000278ab53f000      8K -----   [ anon ]
00000278ab541000    120K rw---   [ anon ]
00000278ab55f000      8K -----   [ anon ]
00000278ab561000    120K rw---   [ anon ]
```

4. 创建地址区间（mmap、do_mmap)，删除地址区间（mummap\do_mummap）


## 整体分配示意图

参考“参考文献” 博客中的图示

<img src="memory1.png" alt="memory" style="zoom:100%;" />

<img src="kernel_memory.jpg" alt="memory1" style="zoom:100%;" />

<img src="memory_manage.jpg" alt="memory2" style="zoom:100%;" />




参考文献

* [博客](https://www.cnblogs.com/alantu2018/p/9177356.html)
* 深入理解linux内核
* Linux 内核设计与实现
