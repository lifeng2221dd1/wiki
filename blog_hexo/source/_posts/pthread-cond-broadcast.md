---
title: pthread_cond_broadcast 会唤醒哪些线程，在broad cast后wait条件变量的线程是否会唤醒？
date: 2021-02-01 10:15:40
tags: pthread
---

我们知道pthread_cond_broadcast 会唤醒那些在等待该条件变量的线程，但是在broad cast之后，再调用wait条件变量的线程是否会唤醒？

我们编写一个测试程序。

```c
#include<stdio.h>
#include<unistd.h>
#include<pthread.h>
#define MAX_THREAD_NUM = PTHREAD_COND_INITIALIZER;
void* thread_fun(void* arg)
{
    int index = *(int*)arg;
    printf("[%d]thread start up!\n", index);
    pthread_mutex_lock(&mutex);
    printf("[%d]thread wait...\n", index);
    pthread_cond_wait(&cond, &mutex);
    printf("[%d]thread wake up\n", index);
    sleep(1);
    pthread_mutex_unlock(&mutex);
    pthread_exit(0);
}
void* thread_fun_other(void* arg)
{
    int index = *(int*)arg;
    printf("[%d]thread start up!\n", index);
    pthread_mutex_lock(&mutex);
    printf("[%d]thread wake up\n", index);
    printf("[%d]sleep 10...\n", index);
    sleep(10);
    pthread_mutex_unlock(&mutex);
    printf("[%d]thread unlock\n", index);
    pthread_exit(0);
}
int main()
{
    pthread_t tid[MAX_THREAD_NUM];
 
    for(int i = 0; i < MAX_THREAD_NUM; i++)
    {
        pthread_create(&tid[i], NULL, thread_fun, &i);
	    sleep(1);
    }
    int p = 100;
    pthread_t p_id;
    pthread_mutex_lock(&mutex);
    pthread_create(&p_id, NULL, thread_fun_other, &p);
    sleep(1);
    printf("start broadcast\n");
    pthread_cond_broadcast(&cond);	//全部唤醒
    pthread_mutex_unlock(&mutex);
    printf("after thread start up!\n");
    pthread_mutex_lock(&mutex);
    printf("after thread wait...\n");
    pthread_cond_wait(&cond, &mutex);
    printf("after thread wake up\n");
    return 0;
}

```

在该程序中，我们首先启动5个线程，等待条件变量，然后启动1个新的线程thread_fun_other，正常等待线程锁。

```sh
~/» ./a.out                                                                                                      
[0]thread start up!
[0]thread wait...
[1]thread start up!
[1]thread wait...
[2]thread start up!
[2]thread wait...
[3]thread start up!
[3]thread wait...
[4]thread start up!
[4]thread wait... #启动5个线程等待条件变量
[100]thread start up! #启动1个线程等待线程锁
start broadcast #唤醒等待在该条件变量上的所有线程
after thread start up! #调用broad cast后，再启动1个线程等待条件变量
[3]thread wake up #唤醒等待条件变量的线程
[100]thread wake up #等待线程锁的线程拿到锁，持有锁10s
[100]sleep 10...
[100]thread unlock
[0]thread wake up#唤醒等待条件变量的线程
[1]thread wake up#唤醒等待条件变量的线程
[2]thread wake up#唤醒等待条件变量的线程
[4]thread wake up#唤醒等待条件变量的线程
after thread wait... #在broad cast后再等待条件变量的线程得不到唤醒
~/» ./a.out #再次执行一次                                                                                                     
[0]thread start up!
[0]thread wait...
[1]thread start up!
[1]thread wait...
[2]thread start up!
[2]thread wait...
[3]thread start up!
[3]thread wait...
[4]thread start up!
[4]thread wait...#启动5个线程等待条件变量
[100]thread start up!#启动1个线程等待线程锁
start broadcast #唤醒等待在该条件变量上的所有线程
after thread start up!#调用broad cast后，再启动1个线程等待条件变量
after thread wait...#调用broad cast后，再启动的线程开始等待条件变量
[100]thread wake up#等待线程锁的线程拿到锁，持有锁10s
[100]sleep 10...
[100]thread unlock
[2]thread wake up#唤醒等待条件变量的线程
[4]thread wake up#唤醒等待条件变量的线程
[3]thread wake up#唤醒等待条件变量的线程
[0]thread wake up#唤醒等待条件变量的线程
[1]thread wake up#唤醒等待条件变量的线程
#没有打印 after thread wake up 在broad cast后再等待条件变量的线程得不到唤醒
```

通过以上验证，我们可以得到结论：pthread_cond_broadcast只会唤醒在pthread_cond_broadcast调用前已经pthread_cond_wait该条件变量的线程，而且在唤醒时，是会进行锁的争用，如果有其他线程尝试拿到线程锁，会在锁释放后，其他等待改条件变量的线程才会得到调用。在pthread_cond_broadcast之后，再wait条件变量的线程得不到唤醒。
