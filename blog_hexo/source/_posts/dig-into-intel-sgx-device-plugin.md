---
title: dig into intel sgx device plugin
date: 2021-05-06 17:21:32
tags:
---

# dig into intel sgx device plugin

最近在看intel的sgx device plugin 插件[github intel device plugins](https://github.com/intel/intel-device-plugins-for-kubernetes)。由于官方的驱动插件仅支持Flexible Launch Control (FLC)的CPU，不支持非FLC的CPU。我本地的magic book 正是老版本的不支持FLC的CPU，我又打算在笔记本上使用k8s部署SGX业务玩一下，那就自己DIY 一下官方的插件，使其支持老版本的CPU。

## intel 官方插件逻辑

```c

    cmd/sgx_plugin                                     manager                                          server
                                                                                
                                                manager.Run()                                 
                                                    1. updatesCh := make(chan updateInfo)
                                                    2. go func(cmd/sgx_plugin/scan())
                                                    3. read from updatesCh and handleUpdate

            
    4. scan()                                    
    5. Notify() send to updatesCh    

                                                    6. handleUpdate()
                                                        6.1 Added
                                                                                                        createServer()  ---->创建server监控更新channel  make(chan map[string]DeviceInfo, 1)

                                                                                                        启动Serve() (向kuberlet注册插件) 

                                                        6.2 update
                                                                                                        向server监控更新channel发送device信息

                                                        6.3 removed   
                                                                                                        停止服务            
```

类图逻辑如下：
<img src="device-plugin.png" alt="device-plugin.png" style="zoom:100%;" />

## 增加非FLC cpu支持

通过上述既有官方实现，可以看到，实现的很漂亮，已经将通用的管理manager以及server处理进行了封装，新增支持非FLC cpu只需要修改cmd/sgx_plugin，增加对应的设备类型即可。修改patch 如下：

```c
        commit 60af9ced114fb92188fd3500b63e1fb386a5fb09
        Author: Li Feng <lifeng2221dd1@zoho.com.cn>
        Date:   Thu May 6 16:27:53 2021 +0800

            isgx: add support isgx for sgx device plugin
            
            steps to build sgx device plugin binary:
            1. cd cmd/sgx_plugin
            2. go env -w GOPROXY=https://goproxy.cn
            3. GO111MODULE=on go build
            
            build sgx device plugin container image:
            
            1. make intel-sgx-plugin
            
            Signed-off-by: Li Feng <lifeng2221dd1@zoho.com.cn>

        diff --git a/cmd/sgx_plugin/sgx_plugin.go b/cmd/sgx_plugin/sgx_plugin.go
        index 99d550f..9c92153 100644
        --- a/cmd/sgx_plugin/sgx_plugin.go
        +++ b/cmd/sgx_plugin/sgx_plugin.go
        @@ -23,6 +23,7 @@ import (
            "strconv"
        
            dpapi "github.com/intel/intel-device-plugins-for-kubernetes/pkg/deviceplugin"
        +	"github.com/klauspost/cpuid/v2"
            "k8s.io/klog/v2"
            pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
        )
        @@ -32,6 +33,7 @@ const (
            namespace                   = "sgx.intel.com"
            deviceTypeEnclave           = "enclave"
            deviceTypeProvision         = "provision"
        +	deviceTypeHuaweiEPC         = "huawei_sgx_epc_MiB"
            devicePath                  = "/dev"
            podsPerCoreEnvVariable      = "PODS_PER_CORE"
            defaultPodCount        uint = 110
        @@ -68,35 +70,63 @@ func (dp *devicePlugin) Scan(notifier dpapi.Notifier) error {
        func (dp *devicePlugin) scan() (dpapi.DeviceTree, error) {
            devTree := dpapi.NewDeviceTree()
        
        -	// Assume that both /dev/sgx_enclave and /dev/sgx_provision must be present.
        -	sgxEnclavePath := path.Join(dp.devfsDir, "sgx_enclave")
        -	sgxProvisionPath := path.Join(dp.devfsDir, "sgx_provision")
        -	if _, err := os.Stat(sgxEnclavePath); err != nil {
        -		klog.Error("No SGX enclave file available: ", err)
        -		return devTree, nil
        -	}
        -	if _, err := os.Stat(sgxProvisionPath); err != nil {
        -		klog.Error("No SGX provision file available: ", err)
        -		return devTree, nil
        -	}
        +	iSgxDevicePath := path.Join(dp.devfsDir, "isgx")
        +	if _, err := os.Stat(iSgxDevicePath); err != nil {
        
        -	deprecatedMounts := []pluginapi.Mount{
        -		{
        -			HostPath:      "/dev/sgx",
        -			ContainerPath: "/dev/sgx",
        -		},
        -	}
        +		// Assume that both /dev/sgx_enclave and /dev/sgx_provision must be present.
        +		sgxEnclavePath := path.Join(dp.devfsDir, "sgx_enclave")
        +		sgxProvisionPath := path.Join(dp.devfsDir, "sgx_provision")
        +		if _, err := os.Stat(sgxEnclavePath); err != nil {
        +			klog.Error("No SGX enclave file available: ", err)
        +			return devTree, nil
        +		}
        +		if _, err := os.Stat(sgxProvisionPath); err != nil {
        +			klog.Error("No SGX provision file available: ", err)
        +			return devTree, nil
        +		}
        
        -	for i := uint(0); i < dp.nEnclave; i++ {
        -		devID := fmt.Sprintf("%s-%d", "sgx-enclave", i)
        -		nodes := []pluginapi.DeviceSpec{{HostPath: sgxEnclavePath, ContainerPath: sgxEnclavePath, Permissions: "rw"}}
        -		devTree.AddDevice(deviceTypeEnclave, devID, dpapi.NewDeviceInfo(pluginapi.Healthy, nodes, deprecatedMounts, nil))
        -	}
        -	for i := uint(0); i < dp.nProvision; i++ {
        -		devID := fmt.Sprintf("%s-%d", "sgx-provision", i)
        -		nodes := []pluginapi.DeviceSpec{{HostPath: sgxProvisionPath, ContainerPath: sgxProvisionPath, Permissions: "rw"}}
        -		devTree.AddDevice(deviceTypeProvision, devID, dpapi.NewDeviceInfo(pluginapi.Healthy, nodes, deprecatedMounts, nil))
        +		deprecatedMounts := []pluginapi.Mount{
        +			{
        +				HostPath:      "/dev/sgx",
        +				ContainerPath: "/dev/sgx",
        +			},
        +		}
        +
        +		for i := uint(0); i < dp.nEnclave; i++ {
        +			devID := fmt.Sprintf("%s-%d", "sgx-enclave", i)
        +			nodes := []pluginapi.DeviceSpec{{HostPath: sgxEnclavePath, ContainerPath: sgxEnclavePath, Permissions: "rw"}}
        +			devTree.AddDevice(deviceTypeEnclave, devID, dpapi.NewDeviceInfo(pluginapi.Healthy, nodes, deprecatedMounts, nil))
        +		}
        +		for i := uint(0); i < dp.nProvision; i++ {
        +			devID := fmt.Sprintf("%s-%d", "sgx-provision", i)
        +			nodes := []pluginapi.DeviceSpec{{HostPath: sgxProvisionPath, ContainerPath: sgxProvisionPath, Permissions: "rw"}}
        +			devTree.AddDevice(deviceTypeProvision, devID, dpapi.NewDeviceInfo(pluginapi.Healthy, nodes, deprecatedMounts, nil))
        +		}
        +	} else {
        +		// get the EPC size
        +		var epcSize uint64
        +		if cpuid.CPU.SGX.Available {
        +			for _, s := range cpuid.CPU.SGX.EPCSections {
        +				epcSize += s.EPCSize
        +			}
        +		}
        +		klog.Infof("epc capacity: %d bytes", epcSize)
        +
        +		deprecatedMounts := []pluginapi.Mount{
        +			{
        +				HostPath:      "/dev/isgx",
        +				ContainerPath: "/dev/isgx",
        +			},
        +		}
        +
        +		sizeMB := epcSize / 1024 / 1024
        +		for i := uint64(0); i < sizeMB; i++ {
        +			devID := fmt.Sprintf("%s-%d", "huawei_sgx_epc_MiB", i)
        +			nodes := []pluginapi.DeviceSpec{{HostPath: iSgxDevicePath, ContainerPath: iSgxDevicePath, Permissions: "rw"}}
        +			devTree.AddDevice(deviceTypeHuaweiEPC, devID, dpapi.NewDeviceInfo(pluginapi.Healthy, nodes, deprecatedMounts, nil))
        +		}
            }
        +
            return devTree, nil
        }

```

主要逻辑：
1. 首先根据/dev/isgx 是否存在来区别是否是FLC CPU，如果是FLC CPU则走原来的逻辑，如果不是FLC，则走新增的逻辑（也就是上述patch中最外层的else分支）
2. 首先获取CPU 的epc 大小，将epc内存根据1M的粒度，虚拟成为不同的逻辑设备
3. 将设备注册为huawei_sgx_epc_MiB，通过devTree.AddDevice，构造devTree


## 使用k8s进行验证

1. 首先部署插件
   
   ```
    # kubectl apply -f ./device_plugin.yaml
   ```

2. 部署使用epc 内存的业务
    
    ```
        # cat enclave.yaml
            .....
            containers:
            - name: hell_lifeng
                image: secgear_hello
                imagePullPolicy: IfNotPresent
                name: helloworld
                resources:
                limits:
                    cpu: 250m
                    memory: 512Mi
                    sgx.intel.com/huawei_sgx_epc_MiB: 2
                volumeMounts:
                - mountPath: /var/run/aesmd/aesm.socket
                name: aesmsocket
            volumes:
            - hostPath:
                path: /var/run/aesmd/aesm.socket
                type: Socket
                name: aesmsocket
            .....
        # kubectl apply -f ./enclave.yaml
        # kubectl get pods -A
            NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
            default       helloworld-d75bf5f9f-fkptp                 1/1     Running   0          150m
            default       helloworld-d75bf5f9f-q5gcj                 1/1     Running   0          150m
            kube-system   calico-kube-controllers-69496d8b75-btf2r   1/1     Running   2          13d
            kube-system   calico-node-6p57t                          1/1     Running   2          13d
            kube-system   coredns-6d56c8448f-5lkrn                   1/1     Running   2          13d
            kube-system   coredns-6d56c8448f-7gbxc                   1/1     Running   2          13d
            kube-system   etcd-lifeng                                1/1     Running   2          13d
            kube-system   kube-apiserver-lifeng                      1/1     Running   3          13d
            kube-system   kube-controller-manager-lifeng             1/1     Running   2          13d
            kube-system   kube-proxy-nvhd9                           1/1     Running   2          13d
            kube-system   kube-scheduler-lifeng                      1/1     Running   2          13d
            kube-system   sgx-device-plugin-ds-wd4w6                 1/1     Running   0          151m

    ```