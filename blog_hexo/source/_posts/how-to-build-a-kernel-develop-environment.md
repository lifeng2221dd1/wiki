---
title: how to build a kernel develop environment
date: 2021-01-12 16:48:46
tags: kernel
---

# 搭建内核开发环境记录

## 下载内核代码以及编译


```sh
linux/main_line/linux ‹master› » git remote -v
cgroup	git://git.kernel.org/pub/scm/linux/kernel/git/tj/cgroup.git (fetch)
cgroup	git://git.kernel.org/pub/scm/linux/kernel/git/tj/cgroup.git (push)
linus	git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git (fetch)
linus	git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git (push)
origin	git@gitee.com:mirrors/linux.git (fetch)
origin	git@gitee.com:mirrors/linux.git (push)
```

下载后，选择需要的内核分支进行编译。

## 搭建验证内核环境

1. 安装qemu
```sh
sudo apt-get install qemu qemu-system-x86
```

2. 使用busybox制作文件系统

```sh
wget https://busybox.net/downloads/busybox-1.33.0.tar.bz2
tar -jxvf busybox-1.33.0.tar.bz2
cd busybox-1.33.0 
make menuconfig 配置static 静态编译
    #
    # Build Options
    #
    CONFIG_STATIC=y

make install

dd if=/dev/zero of=./busybox.img bs=1M count=64
mkfs.ext3 busybox.img
mkdir -p /mnt/disk
mount ./busybox.img /mnt/disk

cp -a ./_install/*  /mnt/disk/
sudo cp -a ./examples/bootfloppy/etc /mnt/disk  
sudo mkdir /mnt/disk/dev  
sudo mkdir /mnt/disk/proc  
sudo mkdir /mnt/disk/etc  
sudo cp -ar ./examples/bootfloppy/etc /mnt/disk/etc  

sudo cp -a /dev/zero /mnt/disk/dev/  
sudo cp -a /dev/console /mnt/disk/dev/  
sudo cp -a /dev/null /mnt/disk/dev/  
sudo cp -a /dev/tty* /mnt/disk/dev/  
sudo cp -a /dev/ttyS0 /mnt/disk/dev/

umount /mnt/disk
```

## 启动验证

```sh
qemu-system-x86_64 -s -kernel arch/x86/boot/bzImage -append "root=/dev/sda" -hda /home/lifeng/linux/busybox.img
```

## 参考资料
- [qemu+busybox](https://blog.51cto.com/chenpiaoping/1529988)

