---
title: dig_into_kernel_cgroup
date: 2021-03-06 10:45:11
tags: cgroup
---

梳理cgroup关键数据结构的关系，备忘。

## struct cgroup_subsys

```c
struct cgroup_subsys {
	struct cgroup_subsys_state *(*css_alloc)(struct cgroup_subsys_state *parent_css);
	int (*css_online)(struct cgroup_subsys_state *css);
	void (*css_offline)(struct cgroup_subsys_state *css);
	void (*css_released)(struct cgroup_subsys_state *css);
	void (*css_free)(struct cgroup_subsys_state *css);
	void (*css_reset)(struct cgroup_subsys_state *css);
	void (*css_rstat_flush)(struct cgroup_subsys_state *css, int cpu);
	int (*css_extra_stat_show)(struct seq_file *seq,
				   struct cgroup_subsys_state *css);

	int (*can_attach)(struct cgroup_taskset *tset);
	void (*cancel_attach)(struct cgroup_taskset *tset);
	void (*attach)(struct cgroup_taskset *tset);
	void (*post_attach)(void);
	int (*can_fork)(struct task_struct *task,
			struct css_set *cset);
	void (*cancel_fork)(struct task_struct *task, struct css_set *cset);
	void (*fork)(struct task_struct *task);
	void (*exit)(struct task_struct *task);
	void (*release)(struct task_struct *task);
	void (*bind)(struct cgroup_subsys_state *root_css);

	bool early_init:1;
    .....
```

### 含义

表示cgroup中的一类子系统的操作接口，如cpuset子系统、io子系统、memory系统。操作系统接口包括申请css(cgroup subsys state)css_alloc等

### 生命周期

系统支持哪些cgroup的子系统，是由初始化时根据系统配置决定。

```c
/* generate an array of cgroup subsystem pointers */
#define SUBSYS(_x) [_x ## _cgrp_id] = &_x ## _cgrp_subsys,
struct cgroup_subsys *cgroup_subsys[] = {
#include <linux/cgroup_subsys.h>
};
#undef SUBSYS
```

比如memory子系统中定义了memory_cgrp_subsys，在初始化时，会根据系统配置，将memory子系统的结构体，注册进入cgroup_subsys 数据结构

```c
struct cgroup_subsys memory_cgrp_subsys = {
	.css_alloc = mem_cgroup_css_alloc,
	.css_online = mem_cgroup_css_online,
	.css_offline = mem_cgroup_css_offline,
	.css_released = mem_cgroup_css_released,
	.css_free = mem_cgroup_css_free,
	.css_reset = mem_cgroup_css_reset,
	.can_attach = mem_cgroup_can_attach,
	.cancel_attach = mem_cgroup_cancel_attach,
	.post_attach = mem_cgroup_move_task,
	.dfl_cftypes = memory_files,
	.legacy_cftypes = mem_cgroup_legacy_files,
	.early_init = 0,
};
```

## struct cgroup_subsys_state

```c
struct cgroup_subsys_state {
	/* PI: the cgroup that this css is attached to */
	struct cgroup *cgroup;

	/* PI: the cgroup subsystem that this css is attached to */
	struct cgroup_subsys *ss;

	/* reference count - access via css_[try]get() and css_put() */
	struct percpu_ref refcnt;

	/* siblings list anchored at the parent's ->children */
	struct list_head sibling;
	struct list_head children;

	/* flush target list anchored at cgrp->rstat_css_list */
	struct list_head rstat_css_node;

	/*
	 * PI: Subsys-unique ID.  0 is unused and root is always 1.  The
	 * matching css can be looked up using css_from_id().
	 */
	int id;

	unsigned int flags;

	/*
	 * Monotonically increasing unique serial number which defines a
	 * uniform order among all csses.  It's guaranteed that all
	 * ->children lists are in the ascending order of ->serial_nr and
	 * used to allow interrupting and resuming iterations.
	 */
	u64 serial_nr;

	/*
	 * Incremented by online self and children.  Used to guarantee that
	 * parents are not offlined before their children.
	 */
	atomic_t online_cnt;

	/* percpu_ref killing and RCU release */
	struct work_struct destroy_work;
	struct rcu_work destroy_rwork;

	/*
	 * PI: the parent css.	Placed here for cache proximity to following
	 * fields of the containing structure.
	 */
	struct cgroup_subsys_state *parent;
};
```

### 含义

用来表示某个cgroup层级当前的各子系统的状态，包括链接到哪个cgroup、属于哪个子系统、引用计数、id等信息。

比如，/sys/fs/cgroup/memory/test/test1， test1、test分别代表的层级，均有自己的cgroup_subsys_state结构体进行表示。


### 生命周期

在 cgroup_subsys 结构体中，我们可以看到，每个子系统实现了对应的css_alloc接口函数，用于创建各子系统自身的cgroup_subsys_state。

```c
struct cgroup_subsys {
	struct cgroup_subsys_state *(*css_alloc)(struct cgroup_subsys_state *parent_css);
```

1. 初始化流程创建

```c
    cgroup_init_early
        --->cgroup_init_subsys
            --->ss->css_alloc(cgroup_css(&cgrp_dfl_root.cgrp, ss))

    cgroup_init
         --->cgroup_init_subsys
            --->ss->css_alloc(cgroup_css(&cgrp_dfl_root.cgrp, ss))
```


2. 新建cgroup层级目录时创建

```c
    cgroup_mkdir
        --->cgroup_apply_control_enable
            --->css_create
                --->css = ss->css_alloc(parent_css);
```

## struct cgroup

```c
struct cgroup {
	/* self css with NULL ->ss, points back to this cgroup */
	struct cgroup_subsys_state self;

	unsigned long flags;		/* "unsigned long" so bitops work */

	/*
	 * The depth this cgroup is at.  The root is at depth zero and each
	 * step down the hierarchy increments the level.  This along with
	 * ancestor_ids[] can determine whether a given cgroup is a
	 * descendant of another without traversing the hierarchy.
	 */
	int level;

	/* Maximum allowed descent tree depth */
	int max_depth;

    ....
    struct cgroup_bpf bpf
    ....
```

### 含义

用来表示某个cgroup层级通用的信息，我的理解该字段与struct cgroup_subsys_state 相比，代表了每个cgroup层级通用的一些信息。
比如level当前层次等。这里比较有意思的一个结构体点，是struct cgroup_bpf bpf， 这里标识这个cgroup 挂接了哪些ebpf程序。
比如 cgroup v2中实现device 设备权限检查，即是使用该bpf进行检查。
检查流程:

```c
devcgroup_check_permission
    ---> BPF_CGROUP_RUN_PROG_DEVICE_CGROUP
        ---> __cgroup_bpf_check_dev_permission
            ---> allow = BPF_PROG_RUN_ARRAY(cgrp->bpf.effective[type], &ctx, BPF_PROG_RUN);
```

另外，在struct cgroup结构里内嵌了struct cgroup_subsys_state self，这里需要注意的是，并不是使用了指针，而是使用了结构体的包含。

### 生命周期

1. 新建cgroup层级目录时创建

```c
    cgroup_mkdir
        --->cgroup_create
```


## struct css_set

```c
/*
 * A css_set is a structure holding pointers to a set of
 * cgroup_subsys_state objects. This saves space in the task struct
 * object and speeds up fork()/exit(), since a single inc/dec and a
 * list_add()/del() can bump the reference count on the entire cgroup
 * set for a task.
 */
struct css_set {
	/*
	 * Set of subsystem states, one for each subsystem. This array is
	 * immutable after creation apart from the init_css_set during
	 * subsystem registration (at boot time).
	 */
	struct cgroup_subsys_state *subsys[CGROUP_SUBSYS_COUNT];

	/* reference count */
	refcount_t refcount;
```

### 含义

包含一组指向各个cgroup_subsys_state对象指针，表示1个cgroup的集合。这个对象是嵌入到task_struct 中的。相当于标识了进程属于cgroup的哪些层级。

```c
struct task_struct {
    ....
#ifdef CONFIG_CGROUPS
	/* Control Group info protected by css_set_lock: */
	struct css_set __rcu		*cgroups;
	/* cg_list protected by css_set_lock and tsk->alloc_lock: */
	struct list_head		cg_list;
#endif
    ....
}
```

### 生命周期

创建css_set的流程是嵌入到进程创建的过程中。调用流程如下：

```c
    copy_process
	    /*
    	 * Ensure that the cgroup subsystem policies allow the new process to be
    	 * forked. It should be noted that the new process's css_set can be changed
    	 * between here and cgroup_post_fork() if an organisation operation is in
    	 * progress.
	    */
	    ---> retval = cgroup_can_fork(p, args);
            --->cgroup_css_set_fork
                --->find_css_set
                    ---->find_existing_css_set /*尝试从既有的css_set_table hash列表中获取*/
                    ---->申请新的css_set 放入css_set_table hash列表
```

## struct cgroup_root

```c
/*
 * A cgroup_root represents the root of a cgroup hierarchy, and may be
 * associated with a kernfs_root to form an active hierarchy.  This is
 * internal to cgroup core.  Don't access directly from controllers.
 */
struct cgroup_root {
	struct kernfs_root *kf_root;

	/* The bitmask of subsystems attached to this hierarchy */
	unsigned int subsys_mask;

	/* Unique id for this hierarchy. */
	int hierarchy_id;

	/* The root cgroup.  Root is destroyed on its release. */
	struct cgroup cgrp;
    .....
}
```

### 含义
cgroup 树的根，初始所有子系统都挂载在全局默认cgrp_dfl_root上。一个cgroup_root可以挂载多个子系统，但是一个子系统，只能挂载在一个cgroup_root上。当mount一个子/多个系统，就会创建一个cgroup_root。

### 生命周期

struct cgroup_root 创建的时机有2个，1个是初始化时，另外1个是在进行子系统的mount时。

1. 初始化
```c
    cgroup_init
        --->BUG_ON(cgroup_setup_root(&cgrp_dfl_root, 0));
```

2. cgroup v1 挂载子系统mount时

```c
    static const struct fs_context_operations cgroup1_fs_context_ops = {
        .free = cgroup_fs_context_free,
        .parse_param = cgroup1_parse_param,
        .get_tree = cgroup1_get_tree,
        .reconfigure = cgroup1_reconfigure,
    };
    cgroup1_get_tree
        --->cgroup1_root_to_use
            --->root = kzalloc(sizeof(*root), GFP_KERNEL);
```
