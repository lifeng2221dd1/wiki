---
title: steps_to_run_container_with_seccgear
date: 2021-03-31 10:19:04
tags: enclave
---

## target

使用secgear SDK编写APP，能够在容器中运行起来。

## TODO Lists

整体方案如下：

<img src="1.png" alt="secgear" style="zoom:100%;" />

<img src="2.png" alt="k8s" style="zoom:100%;" />

开发侧：

* 基础镜像，安装了SGX、Secgear运行所需要的链接库的基础镜像，使用secgear 编译出的二进制、链接库拷贝到该基础镜像就可直接运行.
  
### SGX 基础镜像制作步骤

* 基于intel 官方 sgx基础上制作，在官方Dockerfile中增加build_base_image.sh 脚本

```c

diff --git a/docker/build/Dockerfile b/docker/build/Dockerfile
index ab156f37..1be1202c 100644
--- a/docker/build/Dockerfile
+++ b/docker/build/Dockerfile
@@ -101,3 +101,16 @@ USER sgxuser
 
 CMD ./app
 
+FROM ubuntu:18.04 as base
+RUN apt-get update && apt-get install -y \
+    g++ \
+    libcurl4-openssl-dev \
+    libprotobuf-dev \
+    libssl-dev \
+    make \
+    module-init-tools
+
+WORKDIR /opt/intel
+COPY --from=builder /linux-sgx/linux/installer/bin/*.bin ./
+RUN ./sgx_linux_x64_psw*.bin --no-start-aesm
+RUN sh -c 'echo yes | ./sgx_linux_x64_sdk_*.bin'
diff --git a/docker/build/build_base_image.sh b/docker/build/build_base_image.sh
new file mode 100755
index 00000000..51d596ca
--- /dev/null
+++ b/docker/build/build_base_image.sh
@@ -0,0 +1,6 @@
+#!/bin/sh
+#
+
+set -e
+docker build --target base --build-arg https_proxy=$https_proxy \
+             --build-arg http_proxy=$http_proxy -t sgx_base_hw -f ./Dockerfile ../../
```

* 下载编译基于Intel 的SGX源码仓库修改的代码

```c
# git clone https://gitee.com/lifeng2221dd1/linux-sgx.git
# cd linux-sgx
# make preparation
# cd docker/build
# ./build_base_image.sh
# docker images
# docker tag sgx_base_hw:latest hub.oepkgs.net/lifeng2221dd1/sgx_base_hw:latest
# docker login hub.oepkgs.net
# docker push hub.oepkgs.net/lifeng2221dd1/sgx_base_hw:latest
```

* 业务，在开发环境上，使用secgear 编写、编译出对应的APP，拷贝到基础镜像中，制作业务镜像

```
# Copyright (c) Huawei Technologies Co., Ltd. 2020. All rights reserved.
# secGear is licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.

# steps to build example image:
# 1. build the secGear project at host
#   # git clone https://gitee.com/openeuler/secGear.git
# 	# cd secGear/
# 	# source environment
# 	# source /opt/intel/sgxsdk/environment
# 	# mkdir debug && cd debug
#	# cmake -DCMAKE_BUILD_TYPE=Debug -DCC_SGX=ON -DSGXSDK=/opt/intel/sgxsdk ..
# 	# make 
# 2. build the example image
# 	# cd ../examples/helloworld/
# 	# docker build -t secgear_hello -f ./Dockerfile ../../


FROM hub.oepkgs.net/lifeng2221dd1/sgx_base_hw:latest

COPY debug/lib/sgx/libsgx_0.so /lib64/
COPY debug/lib/libsecgear.so /usr/lib/
COPY debug/examples/helloworld/host/secgear_helloworld /home/
COPY debug/examples/helloworld/enclave/enclave.signed.so /home/

WORKDIR /home

ENTRYPOINT ["/home/secgear_helloworld"]
```


运行侧：

* 节点上安装完成SGX driver、SGX PSW
  
  注意，https://github.com/intel/linux-sgx-driver 中的驱动为旧版本的CPU的驱动，安装后，主机侧看到的设备为/dev/isgx. intel 在推动将sgx的驱动合入上游的linux kernel社区，新版本的驱动存放在 https://github.com/intel/SGXDataCenterAttestationPrimitives/tree/master/driver。建议直接安装新版本的驱动。

* 节点上运行AESM服务，可以通过daemonset 方式拉起
* 阿里/intel 有提供sgx-device-plugin， 实现对SGX节点的EPC内存资源发现、管理、调度

### 安装k8s

如何部署k8s参考附录中安装k8s章节。

```c

# sudo apt-get update && sudo apt-get install -y ca-certificates curl software-properties-common apt-transport-https curl
# curl -s https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | sudo apt-key add -
# sudo tee /etc/apt/sources.list.d/kubernetes.list <<EOF 
deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main
EOF

# sudo apt-get update
# sudo apt-get install -y kubelet=1.19.4-00 kubeadm=1.19.4-00 kubectl=1.19.4-00
# sudo apt-mark hold kubelet kubeadm kubectl

# sudo kubeadm init --cri-socket=/var/run/isulad.sock \
--pod-network-cidr 152.16.0.0/16  \
--image-repository registry.aliyuncs.com/google_containers

# kubectl taint nodes --all node-role.kubernetes.io/master-

# kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml

```


### 在容器中使用sgx 设备

由于机密计算容器需要使用的主机上的/dev/isgx，需要采用k8s的device plugin机制，具体插件机制可以参考附录中的k8s device plugin机制。

### 部署业务

首先以daemonset 方式启动sgx device plugin 插件。

```

# kubectl apply device_plugin.yaml
# cat device_plugin.yaml

apiVersion:  apps/v1
kind: DaemonSet
metadata:
  name: sgx-device-plugin-ds
  namespace: kube-system
spec:
  selector:
    matchLabels:
      k8s-app: sgx-device-plugin
  template:
    metadata:
      annotations:
        scheduler.alpha.kubernetes.io/critical-pod: ""
      labels:
        k8s-app: sgx-device-plugin
    spec:
      containers:
      - image: registry.cn-hangzhou.aliyuncs.com/acs/sgx-device-plugin:v1.0.0-fb467e2-aliyun
        imagePullPolicy: IfNotPresent
        name: sgx-device-plugin
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            drop:
            - ALL
        volumeMounts:
        - mountPath: /var/lib/kubelet/device-plugins
          name: device-plugin
        - mountPath: /dev
          name: dev
      tolerations:
      - effect: NoSchedule
        key: alibabacloud.com/sgx_epc_MiB
        operator: Exists
      volumes:
      - hostPath:
          path: /var/lib/kubelet/device-plugins
          type: DirectoryOrCreate
        name: device-plugin
      - hostPath:
          path: /dev
          type: Directory
        name: dev
```

部署hello-world 业务。

```

# kubectl apply enclave.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: helloworld
  namespace: default
spec:
  replicas: 2
  selector:
    matchLabels:
      app: helloworld
  template:
    metadata:
      labels:
        app: helloworld
    spec:
      containers:
      - name: hell_lifeng
        image: secgear_hello
        imagePullPolicy: IfNotPresent
        name: helloworld
        resources:
          limits:
            cpu: 250m
            memory: 512Mi
            alibabacloud.com/sgx_epc_MiB: 2
        volumeMounts:
        - mountPath: /var/run/aesmd/aesm.socket
          name: aesmsocket
      volumes:
      - hostPath:
          path: /var/run/aesmd/aesm.socket
          type: Socket
        name: aesmsocket
```


## 附录

-[安装k8s](https://www.cnblogs.com/wwph/p/14203626.html)
-[k8s device plugin机制](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/resource-management/device-plugin.md)
