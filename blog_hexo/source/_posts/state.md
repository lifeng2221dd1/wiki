---
title: iSulad 容器状态管理机制解析
date: 2020-08-17 17:26:23
tags: iSulad
---

_作者简介：李峰， 具有多年容器、操作系统软件开发经验，对容器引擎、runtime 等领域有比较深入的研究与理解。深度参与 lxc、containers 等开源容器社区。现在担任 openeuler 轻量级容器引擎 iSulad 社区 maintainer。_

iSulad 是一种容器引擎，容器的管理是其主要的功能之一。iSulad 需要维护好用户创建的所有容器的信息，包括容器的创建时间、当前状态、退出码、退出时间等信息。在用户需要查询这些信息时，向用户反馈正确的容器信息。

## iSulad 容器状态

容器在整个生命周期过程中，其状态跟随用户的操作、容器进程的运行，具有不同的状态。iSulad 所管理的容器，可能存在以下几种状态。

```bash
typedef enum {
    CONTAINER_STATUS_UNKNOWN = 0,
    CONTAINER_STATUS_CREATED = 1,
    CONTAINER_STATUS_STARTING = 2,
    CONTAINER_STATUS_RUNNING = 3,
    CONTAINER_STATUS_STOPPED = 4,
    CONTAINER_STATUS_PAUSED = 5,
    CONTAINER_STATUS_RESTARTING = 6,
    CONTAINER_STATUS_MAX_STATE = 7
} Container_Status;
```

| **状态**   | **状态描述**                                         |
| ---------- | ---------------------------------------------------- |
| created    | 初始化状态，当调用 create 创建容器但未启动的状态     |
| starting   | 正在启动过程中，当容器处于启动过程中时的状态         |
| running    | 运行状态，容器处于运行状态                           |
| exited     | 退出状态，容器由运行状态手动停止或者容器退出后的状态 |
| paused     | 暂停状态，容器处于 FROZEN 冻结状态                   |
| restarting | 容器处于重启状态，当执行 restart policy 时处于该状态 |

## 容器状态变更流程

iSulad 进行容器管理流程如下图所示

<img src="container_stat_lifecycle.png" alt="iSulad-img-single" style="zoom:100%;" />

1. 当收到创建命令后，首先进入创建流程，创建出容器，状态为 created

```bash
# sudo isula create -it busybox sh
4fb2a5823b63718a25fc7572817b4db084d5baf568d1eb8161a135fb3145a6cf
# sudo isula ps -a
CONTAINER ID	IMAGE  	COMMAND	CREATED      	STATUS 	PORTS	NAMES
4fb2a5823b63	busybox	"sh"   	4 seconds ago	Created	     	4fb2a5823b63718a25fc7572817b4db084d5baf568d1eb8161a135fb3145a6cf
```

2. 处于 created 状态的容器，可以通过 start 命令，将其启动起来，启动成功后，容器状态变更为 running 状态，如果启动失败，则容器状态变更为 exited 状态

```bash
# sudo isula start 4fb2a5823b63
# sudo isula ps -a
CONTAINER ID	IMAGE  	COMMAND	CREATED      	STATUS      	PORTS	NAMES
4fb2a5823b63	busybox	"sh"   	6 minutes ago	Up 4 seconds	     	4fb2a5823b63718a25fc7572817b4db084d5baf568d1eb8161a135fb3145a6cf
# sudo isula inspect '--format={{json .State.Pid}}' 4fb2a5823b6
14933
# ps axu | grep 14933
root       14933  0.0  0.0   1308     4 pts/2    Ss+  11:50   0:00 sh
```

3.处于 running 状态的容器，可以通过 pause 命令，挂起指定容器中的所有进程，恢复容器为暂停容器的逆过程，用于恢复被暂停容器中所有进程

```bash
# sudo isula pause 4fb2a5823b63
4fb2a5823b63
# sudo isula ps -a
CONTAINER ID	IMAGE  	COMMAND	CREATED    	STATUS             	PORTS	NAMES
4fb2a5823b63	busybox	"sh"   	2 hours ago	Up 2 hours (Paused)	     	4fb2a5823b63718a25fc7572817b4db084d5baf568d1eb8161a135fb3145a6cf
# ps axu | grep 14933
root       14933  0.0  0.0   1308     4 pts/2    Ds+  11:50   0:00 sh
# sudo isula unpause 4fb2a5823b63
4fb2a5823b63
# sudo isula ps -a
CONTAINER ID	IMAGE  	COMMAND	CREATED    	STATUS    	PORTS	NAMES
4fb2a5823b63	busybox	"sh"   	2 hours ago	Up 2 hours	     	4fb2a5823b63718a25fc7572817b4db084d5baf568d1eb8161a135fb3145a6cf
```

4. 处于 running 状态的容器，可以通过 stop、kill 命令，使容器退出

```bash
# sudo isula stop -t 0 4fb2a5823b63
4fb2a5823b63
# sudo isula ps -a
CONTAINER ID	IMAGE  	COMMAND	CREATED    	STATUS                    	PORTS	NAMES
4fb2a5823b63	busybox	"sh"   	2 hours ago	Exited (137) 4 seconds ago	     	4fb2a5823b63718a25fc7572817b4db084d5baf568d1eb8161a135fb3145a6cf
```

## iSulad 容器状态维护代码实现

iSulad 中维护容器状态的代码位于 src/daemon/modules/container/container_state.c 文件中，其中判断容器状态的逻辑为：

```c
/* state judge status */
Container_Status container_state_judge_status(const container_config_v2_state *state)
{
    if (state == NULL) {
        return CONTAINER_STATUS_UNKNOWN;
    }

    if (state->running) {
        if (state->paused) {
            return CONTAINER_STATUS_PAUSED;
        }
        if (state->restarting) {
            return CONTAINER_STATUS_RESTARTING;
        }

        return CONTAINER_STATUS_RUNNING;
    }

    if (state->starting) {
        return CONTAINER_STATUS_STARTING;
    }

    if (state->started_at == NULL || state->finished_at == NULL) {
        return CONTAINER_STATUS_UNKNOWN;
    }
    if (strcmp(state->started_at, defaultContainerTime) == 0 && strcmp(state->finished_at, defaultContainerTime) == 0) {
        return CONTAINER_STATUS_CREATED;
    }

    return CONTAINER_STATUS_STOPPED;
}
```

从上述代码中，可以看到，iSulad 为每个容器维护了 1 个 container_config_v2_state 结构体，其中包含了该容器的各种状态信息，比如是否处于启动状态、是否已经 running、是否 paused 等信息。然后根据以上信息综合判断得出容器当前的实际状态。
container_config_v2_state 结构体的关键定义如下：

```c
typedef struct {
    bool dead;

    char *error;

    int exit_code;

    char *finished_at;

    bool oom_killed;

    bool paused;

    int pid;

    int p_pid;

    uint64_t start_time;

    uint64_t p_start_time;

    bool removal_inprogress;

    bool restarting;

    bool running;

    char *started_at;

    bool starting;

    defs_health *health;

    yajl_val _residual;
}
container_config_v2_state;
```

因此容器的状态变更，即是对该结构体字段的修改。在文件 src/daemon/modules/api/container_api.h 中定义了对容器状态变更的接口函数。可以通过查询以下接口函数，得到容器状态变更的时机以及条件。

```c
void container_state_set_starting(container_state_t *s);
void container_state_reset_starting(container_state_t *s);
void container_state_set_running(container_state_t *s, const pid_ppid_info_t *pid_info, bool initial);
void container_state_set_stopped(container_state_t *s, int exit_code);
void container_state_set_restarting(container_state_t *s, int exit_code);
void container_state_set_paused(container_state_t *s);
void container_state_reset_paused(container_state_t *s);
```

以设置容器状态为退出为例，可以搜索 container_state_set_stopped 函数调用的地方，即可理解 iSulad 设置容器状态为退出的逻辑。
首先是在 start_container 的逻辑中，当容器启动失败时，需要设置容器状态变更为退出状态。

```c
int start_container(container_t *cont, const char *console_fifos[], bool reset_rm)
{
    ....
set_stopped:
    container_state_set_error(cont->state, (const char *)g_isulad_errmsg);
    util_contain_errmsg(g_isulad_errmsg, &exit_code);
    container_state_set_stopped(cont->state, exit_code); // 设置退出码为实际报错的退出码
    ...
```

另外变更为退出状态的逻辑为，收到容器的退出事件，表明容器已经退出。在函数 container_state_changed 中，处理退出事件，首先判断容器是否有配置重启策略，如果需要重启，则设置状态为 restarting，如果不需要重启，则设置容器的状态为退出状态。

```c
/* container state changed */
static int container_state_changed(container_t *cont, const struct isulad_events_format *events)
{
    ...
            should_restart = restart_manager_should_restart(id, events->exit_status,
                                                            cont->common_config->has_been_manually_stopped,
                                                            time_seconds_since(started_at), &timeout);
            free(started_at);
            started_at = NULL;

            if (should_restart) {
                container_state_set_restarting(cont->state, (int)events->exit_status);
                ...
            } else {
                container_state_set_stopped(cont->state, (int)events->exit_status);
                ...
            }
    ...
```

其他状态的设置流程，可以按照这个思路进行代码分析查看。

## iSulad 容器 GC 机制介绍

在容器的操作、运行过程中，各种不可预知的异常、错误可能会导致 iSulad 暂时失去对容器的管理。比如在容器正在启动的过程中，iSulad daemon 被异常停止，在 iSulad 重新启动后，此时容器的状态可能在 iSulad 停止的过程中发生多次状态变更。可能存在以下几种：

1. 容器启动成功，但是在 iSulad 重新启动之前，容器进程已经执行完成，容器处于退出状态，相关资源未被回收
2. 容器启动失败，根本没有启动成功，容器处于退出状态，相关资源未被回收。
3. 容器启动成功，容器处于运行状态

iSulad 无法区分 1、2 两种状态，为了防止容器资源的泄露，比如 cgroup、post stop hook 未执行等。iSulad 设计了一套 GC 机制，用以回收异常状态的容器资源。iSulad 实现的 GC 机制，是一个常驻线程，名称为 Garbage_collector。

```sh
# ps -T -p `sudo cat /var/run/isulad.pid`                                                                                                      130 ↵
    PID    SPID TTY          TIME CMD
  13075   13075 ?        00:00:00 isulad
  13075   13076 ?        00:00:00 Log_gather
  13075   13079 ?        00:00:00 Shutdown
  13075   13080 ?        00:00:01 Clients_checker
  13075   13081 ?        00:00:00 Monitord
  13075   13082 ?        00:00:00 Supervisor
  13075   13083 ?        00:00:12 Garbage_collect
  13075   13085 ?        00:00:02 isulad
  13075   13086 ?        00:00:00 default-executo
  13075   13087 ?        00:00:00 resolver-execut
  13075   13088 ?        00:00:08 grpc_global_tim
  13075   13089 ?        00:00:00 grpcpp_sync_ser
  13075   13090 ?        00:00:00 isulad
  13075   13158 ?        00:00:00 grpcpp_sync_ser
  13075   13297 ?        00:00:14 grpc_global_tim
```

该线程不断的在遍历需要 GC 的容器链表，尝试进行容器的 GC。

```c
static void *gchandler(void *arg)
{
    ...
    for (;;) {
        gc_containers_lock();

        if (linked_list_empty(&g_gc_containers.containers_list)) {
            gc_containers_unlock();
            goto wait_continue;
        }
        it = linked_list_first_node(&g_gc_containers.containers_list);

        gc_containers_unlock();

        do_gc_container(it);

wait_continue:
        usleep_nointerupt(100 * 1000); /* wait 100 millisecond to check next gc container */
    }
    ...
```

GC 需要回收两部分内容

1. 需要回收容器的父进程，当容器需要进行 GC 时，其父进程已不再需要，如果父进程还存在，则需要将父进程的资源回收。
2. 需要回收容器的资源，包含容器占用的 cgroup 目录，执行 poststophook，在 hook 中用户可能指定执行的资源清理操作。

容器加入 GC 的时机，可以通过搜索 gc_add_container 函数来找到容器何时需要加入 GC 机制。
通过查看代码，可以看到，加入 GC 的机制有以下 2 种情况

1. iSulad 启动时，检查容器当前的状态与持久化在文件中记录的状态不一致时。
   比如在持久化文件中记录的容器状态为 running 状态，但是查询到容器的实际状态为停止状态。说明在 iSulad 未启动的过程中，该容器的状态发生了变更，容器的资源存在未被回收的可能，此时就需要将该容器加入到 GC，尝试回收该容器占用的资源。

   ```c
   static int restore_state(container_t *cont)
   {
    ....
    if (real_status.status == RUNTIME_CONTAINER_STATUS_STOPPED) {
        ret = restore_stopped_container(status, cont, &need_save);
        if (ret != 0) {
            goto out;
        }
    }
    ....
    }

    static int restore_stopped_container(Container_Status status, const container_t *cont, bool *need_save)
    {
        ...
        if (status != CONTAINER_STATUS_STOPPED && status != CONTAINER_STATUS_CREATED) {
            if (util_process_alive(cont->state->state->pid, cont->state->state->start_time)) {
                pid = cont->state->state->pid;
            }
            int nret = post_stopped_container_to_gc(id, cont->runtime, cont->state_path, pid);
            if (nret != 0) {
                ERROR("Failed to post container %s to garbage"
                    "collector, that may lost some resources"
                    "used with container!",
                    id);
            }
            container_state_set_stopped(cont->state, 255);
            *need_save = true;
        }
        ...
    }

   ```

2. 在容器退出时，尝试回收容器的资源，但是容器资源无法回收时，将容器加入到 GC 进行回收。比如容器在运行的过程中，容器的进程进入 D 状态等异常状态时，容器无法正常回收资源。
   举例如下：
   存在以下容器，容器对应进程 PID 为 24368

   ```sh
   # sudo isula ps
   CONTAINER ID	IMAGE  	COMMAND	CREATED    	STATUS      	PORTS	NAMES
   4fb2a5823b63	busybox	"sh"   	7 hours ago	Up 5 seconds	     	4fb2a5823b63718a25fc7572817b4db084d5baf568d1eb8161a135fb3145a6cf
   # sudo isula inspect '--format={{json .State.Pid}}' 4fb2a5823b6
   24368
   ```

   人为构造容器进程处于 D 状态

   ```sh
   # cd /sys/fs/cgroup/freezer
   # mkdir test
   # cd test
   # echo FROZEN > freezer.state
   # echo 24368 > tasks
   # ps axu | grep 24368
   root       24368  0.0  0.0   1308     4 pts/2    Ds+  19:20   0:00 sh
   ```

   停止该容器

   ```sh
   # isula stop -t 0 4fb2a5823b63
   4fb2a5823b63
   ```

   再次尝试操作该容器，由于容器进程未被回收，进入 GC 机制，操作该容器会反馈处于 GC 机制中，无法操作。

   ```sh
   # isula start 4fb2a5823b63
   Error response from daemon: You cannot start container 4fb2a5823b63718a25fc7572817b4db084d5baf568d1eb8161a135fb3145a6cf in garbage collector progress.
   ```

   将容器进程从 D 状态恢复为正常后，容器资源回收后，容器即可重新操作。

   ```sh
    # echo THAWED > freezer.state
    # isula start 4fb2a5823b63
   ```
