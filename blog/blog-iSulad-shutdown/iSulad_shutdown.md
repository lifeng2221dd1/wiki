# iSulad 是如何优雅退出的，shutdown 流程解析

这几天定位了一个停止 iSulad 服务时，iSulad 的资源未被清理干净的问题。问题表现：当使用 systemctl stop isulad 时，镜像完整性校验的标志位 NEED_CHECK 未被清理掉，导致镜像完整性校验逻辑错误。定位后发现是 shutdown 处理线程和主线程之间没有做同步，导致 shutdown 处理线程还未处理完，主线程就退出掉了。这篇博客对 iSulad 的优雅退出流程做个梳理。

## iSulad 优雅退出流程

iSulad 是如何实现优雅退出的呢？可以先看下 iSulad 启动了哪些线程。

```bash
$ ps -T -p `sudo cat /var/run/isulad.pid`
    PID    SPID TTY          TIME CMD
  12431   12431 ?        00:00:00 isulad
  12431   12442 ?        00:00:00 Log_gather
  12431   12446 ?        00:00:00 Shutdown
  12431   12447 ?        00:00:00 Clients_checker
  12431   12448 ?        00:00:00 Monitord
  12431   12449 ?        00:00:00 Supervisor
  12431   12450 ?        00:00:08 Garbage_collect
  12431   12452 ?        00:00:02 isulad
  12431   12453 ?        00:00:00 default-executo
  12431   12454 ?        00:00:00 resolver-execut
  12431   12455 ?        00:00:03 grpc_global_tim
  12431   12456 ?        00:00:00 grpcpp_sync_ser
  12431   12457 ?        00:00:00 isulad
  12431   12510 ?        00:00:00 grpc_global_tim
  12431   12513 ?        00:00:00 grpcpp_sync_ser
```

可以看到 iSulad 启动时，创建了 Shutdown 线程。查看代码

```c
/* shutdown handler */
static void *do_shutdown_handler(void *arg)
{
    int res = 0;

    res = pthread_detach(pthread_self());
    if (res != 0) {
        CRIT("Set thread detach fail");
    }

    prctl(PR_SET_NAME, "Shutdown");

    sem_wait(&g_daemon_shutdown_sem);

    daemon_shutdown();

    return NULL;
}

/* news_shutdown_handler */
int new_shutdown_handler()
{
    int ret = -1;
    pthread_t shutdown_thread;

    INFO("Starting new shutdown handler...");
    ret = pthread_create(&shutdown_thread, NULL, do_shutdown_handler, NULL);
    if (ret != 0) {
        CRIT("Thread creation failed");
        goto out;
    }

    ret = 0;
out:
    return ret;
}
```

iSulad 在启动时，创建一个常驻的线程 Shutdown，用来进行优雅退出时的资源清理。清理的内容包括：

1. 通过调用 grpc 接口中的 shutdown 接口，关闭 iSulad 的 grpc 服务，不再处理新的 grpc 请求。
2. image 镜像管理模块退出，清理镜像管理模块的一些挂载点
3. 卸载 /var/lib/isulad/mnt 目录
4. 删除镜像完整性校验 NEED_CHECK 标志文件、iSulad pid 文件

Shutdown 线程是在 wait 到 g_daemon_shutdown_sem 信号量后，开始处理。在 iSulad 启动时，将 SIGTERM 和 SIGINT 的信号处理函数注册进去。在收到 SIGTERM 或者 SIGINT 信号时，调用对应处理函数，post g_daemon_shutdown_sem 信号量。

```c
static void sigint_handler(int x)
{
    INFO("Got SIGINT; exiting");
    sem_post(&g_daemon_shutdown_sem);
}

static void sigterm_handler(int signo)
{
    INFO("Got SIGTERM; exiting");
    sem_post(&g_daemon_shutdown_sem);
}

static int add_shutdown_signal_handler()
{
....
    sa.sa_handler = sigint_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGINT, &sa, NULL) < 0) {
        ERROR("Failed to add handler for SIGINT");
        return -1;
    }

    (void)memset(&sa, 0, sizeof(struct sigaction));

    sa.sa_handler = sigterm_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGTERM, &sa, NULL) < 0) {
        ERROR("Failed to add handler for SIGTERM");
        return -1;
    }

    return 0;
}
```

## 问题出在哪

遇到的问题，是在 Shutdown 线程与主线程存在退出时机的竞争关系。由于 Shutdown 线程是首先进行 grpc 服务的 shutdown 关闭然后再进行其他的清理动作，而主线程在进行 grpc 服务的 wait 操作。当 grpc shutdown 完成后，主线程的 wait 即返回，主线程会直接退出，Shutdown 线程可能还未完成清理动作，导致 iSulad 资源未清理干净。如下图

<img src="shutdown.png" alt="iSulad-img-single" style="zoom:100%;" />

## 如何解决

在 Shutdown 线程与主线程之间增加同步机制，在 Shutdown 线程清理完后，post 信号量通知主线程，主线程 wait 到该信号后，再退出。
