# docker volume 数据卷

## 实现思想

Docker 提供了数据卷机制用来向容器中提供持久化的数据卷，供容器向其中保存数据。在容器销毁后，数据卷不会跟随容器的销毁而销毁，从而实现了容器数据的保存需求。

实现思想和手动创建文件夹，然后 bind mount 到容器中使用是一致的。

在主机上创建个文件夹，作为保存容器数据使用，创建容器，将创建的目录 bind mount 到容器内使用，尝试将数据保存到该目录下。在主机 host 上查看该文件夹下数据已保存，删除容器，数据不会丢失

```sh
$ cd /tmp
$ mkdir /tmp/dir
$ sudo docker run -it -v /tmp/dir:/tmp/dir busybox sh
/ #
/ # cd /tmp/dir/
/tmp/dir # touch 1
/tmp/dir # touch 2
/tmp/dir # ls
1  2
$ cd /tmp/dir
$ ls
1  2
```

## 使用方法

docker 的 volume 机制，实际上是将上述机制进行了封装，方便用户使用。docker 客户端提供了 volume 命令行用以管理 volume。

```sh
$ sudo docker volume --help

Usage:	docker volume COMMAND

Manage volumes

Commands:
  create      Create a volume # 创建volume
  inspect     Display detailed information on one or more volumes # 查询volume的信息
  ls          List volumes # 列出当前已有的所有volume
  prune       Remove all unused local volumes # 删除所有未使用的本地volume
  rm          Remove one or more volumes  # 删除指定的volume
```

1. 创建 1 个 volume

```sh
$ sudo docker volume create
86ab06e40bf61402a7daea39306a381ea054fc3a4bf56c1bb33cf33efdd6b8fa
```

创建成功后，可以在 docker 的根目录查看该 volume 的文件夹信息。

```sh
$ cd /var/lib/docker/volumes
86ab06e40bf61402a7daea39306a381ea054fc3a4bf56c1bb33cf33efdd6b8fa
$ cd 86ab06e40bf61402a7daea39306a381ea054fc3a4bf56c1bb33cf33efdd6b8fa
$ tree
.
└── _data
```

2. 使用创建好的 volume，映射到容器中使用，并在容器中写入数据

```sh
$ sudo docker run -it -v 86ab06e40bf61402a7daea39306a381ea054fc3a4bf56c1bb33cf33efdd6b8fa:/tmp/dir busybox sh
/ # cd /tmp/dir/
/tmp/dir # ls
/tmp/dir # touch 1
/tmp/dir # touch 2
/tmp/dir # ls
1  2
```

可以查看容器的配置文件，docker 会将该 volume 的路径 bind mount 到容器中。

```sh
$ cat cat /var/run/containerd/io.containerd.runtime.v1.linux/moby/6f310a3d4e6e045b7ce11633a9816bff7f89fe226fa5491bcde2e9c8dc72e67f/config.json| python -m json.tool | less
...
        {
            "destination": "/tmp/dir",
            "type": "bind",
            "source": "/var/lib/docker/volumes/86ab06e40bf61402a7daea39306a381ea054fc3a4bf56c1bb33cf33efdd6b8fa/_data",
            "options": [
                "rbind"
            ]
        },
...
```

在容器外的 volume 目录，可以看到容器中写入到该 volume 的数据

```sh
$ cd /var/lib/docker/volumes
86ab06e40bf61402a7daea39306a381ea054fc3a4bf56c1bb33cf33efdd6b8fa
$ cd 86ab06e40bf61402a7daea39306a381ea054fc3a4bf56c1bb33cf33efdd6b8fa
$ tree
.
└── _data
    ├── 1
    └── 2

1 directory, 2 files
```

## 代码实现分析

docker 的 volume 模块位于目录 docker/volume 下。

1. volume 模块采用分层的设计，模块对外提供的接口定义在 service.go

<img src="service.png" alt="iSulad-img-single" style="zoom:100%;" />

service 中定义了 volume 模块对外的接口，对外提供的接口，供 docker daemon 调用，属于对外的接口层。

2. store.go 为 volume 管理的核心实现，其中包括所有 volume 的启动加载、新增、删除等。

<img src="store.png" alt="iSulad-img-single" style="zoom:100%;" />

3. driver 接口定义了一个 volume 插件需要实现的功能，用以对接到 store。store 中会去调用 driver 的接口实现具体创建功能。

```go
// Driver is for creating and removing volumes.
type Driver interface {
	// Name returns the name of the volume driver.
	Name() string
	// Create makes a new volume with the given name.
	Create(name string, opts map[string]string) (Volume, error)
	// Remove deletes the volume.
	Remove(vol Volume) (err error)
	// List lists all the volumes the driver has
	List() ([]Volume, error)
	// Get retrieves the volume with the requested name
	Get(name string) (Volume, error)
	// Scope returns the scope of the driver (e.g. `global` or `local`).
	// Scope determines how the driver is handled at a cluster level
	Scope() string
}
```

以 create 为例，volume 模块对外提供的接口为

```c
// Create creates a volume
func (s *VolumesService) Create(ctx context.Context, name, driverName string, opts ...opts.CreateOption) (*types.Volume, error) {
	if name == "" {
		name = stringid.GenerateNonCryptoID()
	}
	v, err := s.vs.Create(ctx, name, driverName, opts...)
	if err != nil {
		return nil, err
	}

	s.eventLogger.LogVolumeEvent(v.Name(), "create", map[string]string{"driver": v.DriverName()})
	apiV := volumeToAPIType(v)
	return &apiV, nil
}
```

该接口中会调用 store 中的 create 接口。store 中会根据 driver 名称，调用不同 driver 的接口,创建完成后，写入 volume 元数据。

```c
func (s *VolumeStore) create(ctx context.Context, name, driverName string, opts, labels map[string]string) (volume.Volume, error) {
    ....
	// Since there isn't a specified driver name, let's see if any of the existing drivers have this volume name
	if driverName == "" {
		v, _ = s.getVolume(ctx, name, "")
		if v != nil {
			return v, nil
		}
	}

	if driverName == "" {
		driverName = volume.DefaultDriverName
	}
	vd, err := s.drivers.CreateDriver(driverName)
	if err != nil {
		return nil, &OpErr{Op: "create", Name: name, Err: err}
	}
    ...
```

当不指定 driver 时，默认使用 local。local 的 create 较为简单，创建对应的\_data 目录。

```c
func (r *Root) Create(name string, opts map[string]string) (volume.Volume, error) {
	if err := r.validateName(name); err != nil {
		return nil, err
	}

	r.m.Lock()
	defer r.m.Unlock()

	v, exists := r.volumes[name]
	if exists {
		return v, nil
	}

	path := r.DataPath(name)
	if err := idtools.MkdirAllAndChown(path, 0755, r.rootIdentity); err != nil {
		return nil, errors.Wrapf(errdefs.System(err), "error while creating volume path '%s'", path)
	}
```
