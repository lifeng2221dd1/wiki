
#include<stdio.h>
#include<unistd.h>
#include<pthread.h>
#define MAX_THREAD_NUM 5
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
void* thread_fun(void* arg)
{
    int index = *(int*)arg;
    printf("[%d]thread start up!\n", index);
    pthread_mutex_lock(&mutex);
    printf("[%d]thread wait...\n", index);
    pthread_cond_wait(&cond, &mutex);
    printf("[%d]thread wake up\n", index);
    sleep(1);
    pthread_mutex_unlock(&mutex);
    pthread_exit(0);
}
void* thread_fun_other(void* arg)
{
    int index = *(int*)arg;
    printf("[%d]thread start up!\n", index);
    pthread_mutex_lock(&mutex);
    printf("[%d]thread wake up\n", index);
    printf("[%d]sleep 10...\n", index);
    sleep(10);
    pthread_mutex_unlock(&mutex);
    printf("[%d]thread unlock\n", index);
    pthread_exit(0);
}
int main()
{
    pthread_t tid[MAX_THREAD_NUM];
 
    for(int i = 0; i < MAX_THREAD_NUM; i++)
    {
        pthread_create(&tid[i], NULL, thread_fun, &i);
	sleep(1);
    }
    int p = 100;
    pthread_t p_id;
    pthread_mutex_lock(&mutex);
    pthread_create(&p_id, NULL, thread_fun_other, &p);
    sleep(1);
    printf("start broadcast\n");
    pthread_cond_broadcast(&cond);	//全部唤醒
    pthread_mutex_unlock(&mutex);
    printf("after thread start up!\n");
    pthread_mutex_lock(&mutex);
    printf("after thread wait...\n");
    pthread_cond_wait(&cond, &mutex);
    printf("after thread wake up\n");
    return 0;
}
