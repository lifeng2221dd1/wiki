#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

__thread char *g_errmsg = NULL;

void *thr_fn(void *arg)
{
    int n = 1;
    while(n--) {
        printf("thread count %d %p %s", n, g_errmsg, g_errmsg);
        sleep(1);
    }
    printf("thread exit\n");
    return (void *)1;
}


int main()
{    
    
    pthread_t tid;
    void *retval;
    int err;

    g_errmsg = strdup("xxxxx");

    pthread_create(&tid, NULL, thr_fn, NULL);
    getchar();

    pthread_detach(tid);
        
    return 0;
}
