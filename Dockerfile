    FROM node
    MAINTAINER Haozi007

    # Create hexo work dir
    WORKDIR /blog
    RUN npm install -g hexo-cli
    RUN hexo init /blog
    EXPOSE 4000

    CMD hexo generate && hexo server
